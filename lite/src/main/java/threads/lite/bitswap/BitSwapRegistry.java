package threads.lite.bitswap;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.quic.Connection;

// TODO [Future urgent] replace by virtual threads
final class BitSwapRegistry extends ConcurrentHashMap<Cid, ScheduledExecutorService> implements AutoCloseable {

    private final ConcurrentHashMap<Cid, Integer> delays = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Cid, List<PeerId>> sends = new ConcurrentHashMap<>();

    BitSwapRegistry() {
        super();
    }

    void register(@NonNull Cid cid) {
        this.put(cid, Executors.newSingleThreadScheduledExecutor());
        this.delays.put(cid, 0);
        this.sends.put(cid, new ArrayList<>());
    }

    void unregister(@NonNull Cid cid) {
        ScheduledExecutorService timer = this.remove(cid);
        if (timer != null) {
            timer.shutdown();
        }
        this.delays.remove(cid);
        this.sends.remove(cid);
    }

    boolean isRegistered(@NonNull Cid cid) {
        return this.containsKey(cid);
    }


    private boolean hasSend(@NonNull Cid cid, @NonNull PeerId peerId) {
        List<PeerId> peerIds = sends.get(cid);
        Objects.requireNonNull(peerIds);
        if (peerIds.contains(peerId)) {
            return true;
        } else {
            peerIds.add(peerId);
            return false;
        }
    }

    void scheduleWants(@NonNull Connection connection, @NonNull Cid cid,
                       @NonNull Runnable runnable) {

        ScheduledExecutorService timer = this.get(cid);
        if (timer != null) {

            if (hasSend(cid, connection.remotePeerId())) {
                return;
            }

            int delay = delays.computeIfAbsent(cid, cid1 -> 0);

            timer.schedule(runnable, delay, TimeUnit.MILLISECONDS);

            delays.put(cid, delay + (IPFS.BITSWAP_REQUEST_DELAY * 1000));
        }

    }

    @Override
    public void close() {
        try {
            values().forEach(ExecutorService::shutdown);
        } catch (Throwable throwable) {
            LogUtils.error(BitSwapRegistry.class.getSimpleName(), throwable);
        }
        delays.clear();
        sends.clear();
        this.clear();
    }
}
