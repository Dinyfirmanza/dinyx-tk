package threads.lite.bitswap;

import androidx.annotation.NonNull;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.DataHandler;
import threads.lite.quic.Connection;
import threads.lite.quic.StreamRequester;


public final class BitSwapEngine {
    private static final String TAG = BitSwapEngine.class.getSimpleName();
    private final BlockStore blockstore;

    // TODO [Future urgent] replace by virtual threads
    private final ExecutorService service = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors());
    private final Map<Task, Future<?>> tasks = new ConcurrentHashMap<>();
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public BitSwapEngine(@NonNull BlockStore blockStore) {
        this.blockstore = blockStore;
    }

    private static CompletableFuture<Void> sendReply(@NonNull Connection connection,
                                                     @NonNull MessageOuterClass.Message message,
                                                     int timeout) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        try {
            StreamRequester.createStream(connection, new BitSwapRequest(done))
                    .request(DataHandler.encode(message,
                            IPFS.MULTISTREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL), timeout);
        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }
        return done;
    }

    public void close() {
        closed.set(true);
        try {
            service.shutdown();
            boolean termination = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            tasks.clear();
        }
    }

    private boolean isClosed() {
        return closed.get();
    }

    void receiveMessage(@NonNull Connection connection, @NonNull MessageOuterClass.Message bsm) {

        if (isClosed()) {
            return;
        }
        PeerId remotePeerId = connection.remotePeerId();
        BitSwapMessage.evaluateResponse(blockstore, bsm, cid -> {
            // this is for cancel task
            Task task = new Task(remotePeerId, cid, Type.BLOCK);
            removeTask(task);
        }, cid -> {
            // this is for a block message
            Task task = new Task(remotePeerId, cid, Type.BLOCK);
            if (!tasks.containsKey(task)) {
                tasks.put(task, service.submit(() -> {
                    try {
                        MessageOuterClass.Message message =
                                BitSwapMessage.createBlockMessage(blockstore, cid);
                        sendReply(connection, message, IPFS.GRACE_PERIOD)
                                .get(IPFS.GRACE_PERIOD, TimeUnit.SECONDS); // can take time
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        removeTask(task);
                    }
                }));
            }
        }, cid -> {
            // this is for create dont have messages
            Task task = new Task(remotePeerId, cid, Type.DONT_HAVE);
            if (!tasks.containsKey(task)) {
                tasks.put(task, service.submit(() -> {
                    try {
                        MessageOuterClass.Message message = BitSwapMessage.createDontHave(cid);
                        sendReply(connection, message, IPFS.DEFAULT_REQUEST_TIMEOUT)
                                .get(IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);  // should go fast
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        removeTask(task);
                    }
                }));
            }
        }, cid -> {
            // this is for create have messages
            Task task = new Task(remotePeerId, cid, Type.HAVE);
            if (!tasks.containsKey(task)) {
                tasks.put(task, service.submit(() -> {
                    try {
                        MessageOuterClass.Message message = BitSwapMessage.createHave(cid);
                        sendReply(connection, message, IPFS.DEFAULT_REQUEST_TIMEOUT).get(
                                IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS); // should go fast
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        removeTask(task);
                    }
                }));
            }
        });

    }

    private void removeTask(Task task) {
        try {
            Future<?> future = tasks.remove(task);
            if (future != null) {
                if (!future.isCancelled()) {
                    future.cancel(true);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public enum Type {
        BLOCK, HAVE, DONT_HAVE
    }

    record Task(PeerId peerId, Cid cid, Type type) {
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Task task = (Task) o;
            return peerId.equals(task.peerId) && cid.equals(task.cid) && type == task.type;
        }

        @Override
        public int hashCode() {
            return Objects.hash(peerId, cid, type); // ok, checked, maybe opt
        }
    }

}
