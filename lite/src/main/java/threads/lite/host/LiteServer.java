package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.Autonat;
import threads.lite.autonat.AutonatService;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.bitswap.BitSwapEngineHandler;
import threads.lite.cid.Cid;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.AutonatResult;
import threads.lite.core.Cancellable;
import threads.lite.core.MultistreamHandler;
import threads.lite.core.NatType;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.dht.DhtKademlia;
import threads.lite.holepunch.HolePunchHandler;
import threads.lite.ident.IdentityHandler;
import threads.lite.ident.IdentityPushHandler;
import threads.lite.ident.IdentityService;
import threads.lite.quic.AlpnLibp2pResponder;
import threads.lite.quic.AlpnLiteResponder;
import threads.lite.quic.Connection;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.quic.Parameters;
import threads.lite.quic.ServerConnector;
import threads.lite.quic.Settings;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamData;
import threads.lite.quic.StreamResponder;
import threads.lite.quic.Version;
import threads.lite.relay.RelayService;
import threads.lite.relay.RelayStopHandler;
import threads.lite.swap.LiteSwapHandler;

public final class LiteServer implements Server {
    private static final String TAG = LiteServer.class.getSimpleName();

    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final AtomicReference<NatType> natType = new AtomicReference<>();
    @NonNull
    private final AtomicReference<Multiaddr> dialableAddress = new AtomicReference<>();
    @NonNull
    private final AtomicReference<Multiaddr> observedAddress = new AtomicReference<>();
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final LiteResponder liteResponder;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final ConcurrentHashMap<PeerId, Connection> swarm = new ConcurrentHashMap<>();
    @NonNull
    private final AtomicReference<IdentifyOuterClass.Identify> identify = new AtomicReference<>();
    @NonNull
    private final IdentifyUpdater identifyUpdater = new IdentifyUpdater();

    LiteServer(@NonNull LiteHost host, @NonNull DatagramSocket socket) {
        this.host = host;
        this.socket = socket;

        BitSwapEngine bitSwapEngine = new BitSwapEngine(host.getBlockStore());

        Map<String, ProtocolHandler> protocols = Map.of(
                IPFS.MULTISTREAM_PROTOCOL, new MultistreamHandler(),
                IPFS.LITE_PUSH_PROTOCOL, new LitePushHandler(host),
                IPFS.LITE_PULL_PROTOCOL, new LitePullHandler(host),
                IPFS.LITE_SWAP_PROTOCOL, new LiteSwapHandler(host),
                IPFS.IDENTITY_PROTOCOL, new IdentityHandler(this),
                IPFS.IDENTITY_PUSH_PROTOCOL, new IdentityPushHandler(this),
                IPFS.BITSWAP_PROTOCOL, new BitSwapEngineHandler(bitSwapEngine),
                IPFS.RELAY_PROTOCOL_STOP, new RelayStopHandler(),
                IPFS.HOLE_PUNCH_PROTOCOL, new HolePunchHandler(this));
        this.liteResponder = new LiteResponder(protocols);
        this.serverConnector = createServerConnector(socket, liteResponder);
    }

    private static int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    @Override
    @NonNull
    public LiteResponder getLiteResponder() {
        return liteResponder;
    }

    @Override
    public byte[] getPrivateKey() {
        return host.getKeys().privateKey();
    }

    @NonNull
    private ServerConnector createServerConnector(DatagramSocket socket, StreamResponder streamResponder) {
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.QUIC_version_1);

        Map<String, Function<Stream, Consumer<StreamData>>> streamDataConsumers =
                Map.of(Settings.LIBP2P_ALPN,
                        quicStream -> AlpnLibp2pResponder.create(streamResponder),
                        Settings.LITE_ALPN,
                        quicStream -> AlpnLiteResponder.create(streamResponder));

        return new ServerConnector(socket, new LiteTrust(), host.getLiteCertificate().certificate(),
                host.getLiteCertificate().privateKey(), supportedVersions, streamDataConsumers);

    }

    @SuppressWarnings("unused")
    @Override
    public void closeConnection(@Nullable Connection connection) {
        try {
            if (connection != null) {
                connection.close();
                swarm.remove(connection.remotePeerId());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Nullable
    @Override
    public Connection getConnection(PeerId peerId) {
        Connection connection = swarm.get(peerId);
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            } else {
                swarm.remove(peerId);
            }
        }
        return null;
    }

    @Override
    public void addSwarmConnection(Connection connection) {
        swarm.put(connection.remotePeerId(), connection);
    }

    @NonNull
    public Set<Connection> swarm() {
        Set<Connection> result = new HashSet<>();
        for (Connection connection : swarm.values()) {
            if (connection.isConnected()) {
                result.add(connection);
            } else {
                swarm.remove(connection.remotePeerId());
            }
        }
        return result;
    }

    @Override
    public Crypto.PublicKey getPublicKey() {
        return host.getPublicKey();
    }

    @Override
    public Multiaddrs publishMultiaddrs() {
        Multiaddrs set = new Multiaddrs();
        Multiaddr dialable = dialableAddress.get();
        if (dialable != null) {
            set.add(dialable);
        } else {
            set.addAll(publicNetworkAddresses());
        }
        return set;
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }

    @Override
    public void shutdown() {
        swarm.values().forEach(Connection::close);
        serverConnector.shutdown();
        swarm.clear();
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public int numServerConnections() {
        return serverConnector.numConnections();
    }

    private void punching(Multiaddr multiaddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = multiaddr.inetAddress();
            int port = multiaddr.port();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            socket.send(datagram);
            LogUtils.debug(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(multiaddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    public void holePunching(@NonNull Multiaddrs multiaddrs) {
        try {
            // TODO [Future urgent] replace by virtual threads
            ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
            for (Multiaddr multiaddr : multiaddrs) {
                service.execute(() -> punching(multiaddr));
            }
            boolean finished = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    ServerConnector getServerConnector() {
        return serverConnector;
    }

    @Override
    public PeerId self() {
        return host.self();
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return host.getRoutingPeers();
    }

    @NonNull
    @Override
    public PeerStore getPeerStore() {
        return host.getPeerStore();
    }

    @NonNull
    public Connection connect(Multiaddr multiaddr, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {
        Connection connection = getConnection(multiaddr.peerId());
        if (connection != null) {
            return connection;
        }
        connection = ConnectionBuilder.connect(liteResponder, multiaddr, parameters,
                host.getLiteCertificate(), host.ipv());
        addSwarmConnection(connection);
        return connection;
    }

    @Override
    public Map<String, ProtocolHandler> getProtocols() {
        return liteResponder.protocols();
    }

    @Override
    public LiteCertificate getLiteCertificate() {
        return host.getLiteCertificate();
    }

    @NonNull
    public Set<String> getProtocolNames() {
        return liteResponder.getProtocolNames();
    }

    @NonNull
    public Set<Reservation> reservations() {
        Set<Reservation> result = new HashSet<>();
        for (Reservation reservation : reservations) {
            try {
                if (reservation.connection().isConnected()) {
                    result.add(reservation);
                } else {
                    reservations.remove(reservation);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return result;
    }

    @NonNull
    private Set<Multiaddr> shuffle() {
        List<Reservation> reservationList = new ArrayList<>(reservations());
        Collections.shuffle(reservationList); // shuffle the list, so that new values will be defined
        return reservationList.stream()
                .map(Reservation::circuitAddress)
                .collect(Collectors.toSet());
    }

    @NonNull
    private Set<Reservation> reservations(@NonNull Collection<Multiaddr> multiaddrs,
                                          @NonNull Cancellable cancellable) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = ConcurrentHashMap.newKeySet();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.connection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }

                if (reservation.limit().limited()) {
                    if (reservation.expireInMinutes() > 2) {
                        relaysStillValid.add(reservation.getRelayId());
                    } else {
                        list.remove(reservation);
                    }
                } else { // static relay
                    // still valid
                    relaysStillValid.add(reservation.getRelayId());
                }
            }

            if (!multiaddrs.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : multiaddrs) {
                    service.execute(() -> {
                        try {
                            if (cancellable.isCancelled()) {
                                return;
                            }
                            PeerId peerId = address.peerId();
                            Objects.requireNonNull(peerId);

                            if (!relaysStillValid.contains(peerId)) {
                                reservation(address);
                            } // else case: nothing to do here, reservation is sill valid

                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable.getClass().getSimpleName() + " "
                                    + throwable.getMessage());
                        }
                    });

                }
                service.shutdown();
                if (cancellable.timeout() > 0) {
                    try {
                        boolean termination = service.awaitTermination(
                                cancellable.timeout(), TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    @Override
    @NonNull
    public Reservation reservation(@NonNull Multiaddr multiaddr) throws Exception {
        return RelayService.reservation(this, multiaddr);
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public void reservations(Cancellable cancellable) {
        try {

            List<Multiaddr> swarm = host.getSwarmStore().getMultiaddrs();

            Set<Reservation> reservations = reservations(swarm, cancellable);

            // cleanup swarm begin
            Set<Multiaddr> relays = ConcurrentHashMap.newKeySet();
            for (Reservation reservation : reservations) {
                relays.add(reservation.relayAddress());
            }
            for (Multiaddr address : swarm) {
                if (!relays.contains(address)) {
                    host.getSwarmStore().removeMultiaddr(address);
                }
            }
            // cleanup swarm end

            if (cancellable.isCancelled()) {
                return;
            }


            try (DhtKademlia dhtKademlia = new DhtKademlia(this)) {

                // fill up reservations [not yet enough]
                dhtKademlia.findClosestPeers(cancellable, multiaddr -> {

                    if (relays.contains(multiaddr)) {
                        return;
                    }

                    if (cancellable.isCancelled()) {
                        return;
                    }

                    try {
                        host.getSwarmStore().storeMultiaddr(
                                reservation(multiaddr).relayAddress());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable.getClass().getSimpleName());
                    }
                }, self());

            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void swarm(Cancellable cancellable) {
        try {

            Set<PeerId> handled = new HashSet<>();

            // get swarm sorts out the not anymore connected connections
            swarm().forEach(connection -> handled.add(connection.remotePeerId()));


            if (cancellable.isCancelled()) {
                return;
            }

            List<Multiaddr> swarm = host.getSwarmStore().getMultiaddrs();
            swarm.forEach(multiaddr -> {
                if (handled.contains(multiaddr.peerId())) {
                    return;
                }

                if (cancellable.isCancelled()) {
                    return;
                }

                try {
                    connect(multiaddr, Parameters.getDefault(true));
                    host.getSwarmStore().storeMultiaddr(multiaddr);
                } catch (Throwable throwable) {
                    host.getSwarmStore().removeMultiaddr(multiaddr);
                    LogUtils.error(TAG, throwable);
                }
            });


            try (DhtKademlia dhtKademlia = new DhtKademlia(this)) {

                // fill up reservations [not yet enough]
                dhtKademlia.findClosestPeers(cancellable,
                        multiaddr -> {

                            if (handled.contains(multiaddr.peerId())) {
                                return;
                            }

                            if (cancellable.isCancelled()) {
                                return;
                            }

                            try {
                                connect(multiaddr, Parameters.getDefault(true));
                                host.getSwarmStore().storeMultiaddr(multiaddr);
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }, self());
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    public AutonatResult autonat() {
        return autonat(host.getBootstrap(), IPFS.AUTONAT_TIMEOUT);
    }

    @NonNull
    public AutonatResult autonat(Collection<Multiaddr> multiaddrs, int timeout) {
        autonat.lock();
        try (Session session = host.createSession(host.getBlockStore(), cid -> false)) {
            Autonat autonat = new Autonat();
            Set<Multiaddr> addresses = publicNetworkAddresses();

            AutonatService.autonat(session, addresses, autonat, multiaddrs, timeout);

            natType.set(autonat.getNatType());
            Multiaddr winner = autonat.winner();
            if (winner != null) {
                dialableAddress.set(winner);
                identify.set(null); // addresses have changed
            }

            InetAddress inetAddress = autonat.getInetAddress();
            if (inetAddress != null) {
                observedAddress.set(Multiaddr.create(self(), inetAddress, getPort()));
            }
            return new AutonatResult(autonat.getNatType(), winner);
        } finally {
            autonat.unlock();
        }
    }

    @NonNull
    public Set<Multiaddr> publicNetworkAddresses() {
        Set<Multiaddr> set = new HashSet<>();

        int port = getPort();

        for (InetAddress inetAddress : Network.networkAddresses()) {
            try {
                set.add(Multiaddr.create(self(), new InetSocketAddress(inetAddress, port)));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        return set;
    }

    @Override
    public void updateNetwork() {
        dialableAddress.set(null);
        observedAddress.set(null);
        identify.set(null);
        host.updateNetwork();
    }

    @NonNull
    public PeerInfo identity() throws Exception {
        return IdentityService.getPeerInfo(self(), identify(), self());

    }

    @NonNull
    public Multiaddrs dialableAddresses() {
        Multiaddrs set = new Multiaddrs();
        try {
            set.addAll(publicNetworkAddresses());
            int port = getPort();
            set.addAll(Multiaddr.getSiteLocalAddresses(self(), port));
            // shuffle the list, so that new values will be defined
            //noinspection SimplifyStreamApiCallChains
            set.addAll(shuffle().stream().limit(Integer.MAX_VALUE).collect(Collectors.toList()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable.getClass().getSimpleName());
        }
        return set;
    }

    public void provide(@NonNull Cid cid, @NonNull Consumer<Multiaddr> consumer,
                        @NonNull Cancellable cancellable) {

        try (DhtKademlia dhtKademlia = new DhtKademlia(this)) {
            dhtKademlia.provide(cancellable, this, consumer, cid);
        }
    }

    @NonNull
    @Override
    public NatType getNatType() {
        return natType.get();
    }

    @Nullable
    @Override
    public Multiaddr getObserved() {
        return observedAddress.get();
    }

    @Override
    public IdentifyOuterClass.Identify identify() {
        return identify.updateAndGet(identifyUpdater);
    }

    @Override
    public Supplier<IPV> ipv() {
        return host.ipv();
    }

    private class IdentifyUpdater implements UnaryOperator<IdentifyOuterClass.Identify> {
        @Override
        public IdentifyOuterClass.Identify apply(IdentifyOuterClass.Identify identify) {
            if (identify == null) {
                return IdentityService.createOwnIdentity(
                        self(), getPrivateKey(), getPublicKey(),
                        getProtocolNames(), publishMultiaddrs());
            }
            return identify;
        }
    }

}
