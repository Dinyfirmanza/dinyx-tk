package threads.lite.host;

import androidx.annotation.NonNull;

import java.util.Objects;

import record.pb.EnvelopeOuterClass;
import threads.lite.IPFS;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Settings;
import threads.lite.quic.Stream;

public final class LitePushHandler implements ProtocolHandler {

    private final LiteHost host;

    LitePushHandler(@NonNull LiteHost host) {
        this.host = host;
    }


    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.LITE_PUSH_PROTOCOL), true);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        EnvelopeOuterClass.Envelope envelope =
                EnvelopeOuterClass.Envelope.parseFrom(data);
        if (Objects.equals(alpn, Settings.LITE_ALPN)) {
            stream.response(); // finish stream
        }
        host.push(stream.connection(), envelope);

    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
    }

}
