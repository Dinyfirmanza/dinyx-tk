package threads.lite.host;


import record.pb.EnvelopeOuterClass;
import threads.lite.quic.Connection;

public record LitePush(Connection connection, EnvelopeOuterClass.Envelope envelope) {
}
