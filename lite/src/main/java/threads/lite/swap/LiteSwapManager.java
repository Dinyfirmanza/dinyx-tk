package threads.lite.swap;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import merkledag.pb.Merkledag;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.cid.Prefix;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.lite.core.Swap;
import threads.lite.quic.Connection;


public final class LiteSwapManager implements Swap {

    private static final String TAG = LiteSwapManager.class.getSimpleName();
    @NonNull
    private final Session session;
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public LiteSwapManager(@NonNull Session session) {
        this.session = session;
    }

    @Override
    public void close() {
        try {
            closed.set(true);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @NonNull
    private Block swapSwarm(@NonNull Cancellable cancellable, @NonNull Cid cid) throws Exception {

        Set<PeerId> haves = new HashSet<>();

        swap.pb.Swap.SwapRequest swapRequest =
                swap.pb.Swap.SwapRequest.newBuilder().setBlock(
                        ByteString.copyFrom(cid.encoded())).build();

        long request = System.currentTimeMillis();

        // note this one runs forever, until it is cancelled
        while (!cancellable.isCancelled()) {

            if (System.currentTimeMillis() > (request + IPFS.GRACE_PERIOD * 2 * 1000)) { // 30 secs
                throw new Exception("no successfully request"); // last request is 30 secs ago, no success
            }

            for (Connection connection : session.swarm()) {

                if (haves.add(connection.remotePeerId())) {

                    LogUtils.info(TAG, "Schedule Get " +
                            cid + " " + connection.remoteAddress());

                    try {
                        request = System.currentTimeMillis();
                        swap.pb.Swap.SwapResponse swapResponse = LiteSwapService.swap(
                                connection, swapRequest);

                        if (swapResponse.getStatus() == swap.pb.Swap.SwapResponse.Status.Have) {

                            swap.pb.Swap.SwapResponse.Block payload = swapResponse.getBlock();
                            ByteString prefix = payload.getPrefix();
                            ByteString payloadData = payload.getData();
                            Prefix pref = Prefix.decode(prefix.toByteArray());
                            Cid cmp = Prefix.sum(pref, payloadData.toByteArray());

                            if (!Objects.equals(cmp, cid)) {
                                throw new Exception("Invalid Cid block returned");
                            }

                            Block received = Block.createBlock(cmp,
                                    Merkledag.PBNode.parseFrom(payloadData.toByteArray()));
                            session.getBlockStore().storeBlock(received);
                            return received;

                        } // ignore DontHave

                    } catch (Throwable throwable) {
                        // give connection another try
                        haves.remove(connection.remotePeerId());
                        LogUtils.error(TAG, throwable);
                    }
                }


            }
        }
        if (cancellable.isCancelled()) {
            throw new InterruptedException("canceled operation");
        }
        throw new Exception("Block not found");
    }


    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {

        if (isClosed()) throw new IllegalStateException("Bitswap is closed");

        LogUtils.info(TAG, "Block Get " + cid);

        return swapSwarm(() -> cancellable.isCancelled() || isClosed(), cid);

    }


    private boolean isClosed() {
        return closed.get();
    }

}
