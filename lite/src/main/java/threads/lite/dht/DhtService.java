package threads.lite.dht;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import dht.pb.Dht;
import record.pb.RecordOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.Server;
import threads.lite.quic.Connection;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;
import threads.lite.quic.TransportError;

public interface DhtService {

    String TAG = DhtService.class.getSimpleName();

    @NonNull
    static Dht.Message createGetValueMessage(byte[] key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_VALUE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
    }


    @NonNull
    static Dht.Message createFindNodeMessage(byte[] key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    static Dht.Message createGetProvidersMessage(Multihash multihash) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(multihash.encoded()))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    static Dht.Message createPutValueMessage(RecordOuterClass.Record record) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.PUT_VALUE)
                .setKey(record.getKey())
                .setRecord(record)
                .setClusterLevelRaw(0).build();
    }


    @NonNull
    static Dht.Message createAddProviderMessage(Server server, Multihash multihash) {

        Dht.Message.Builder builder = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.ADD_PROVIDER)
                .setKey(ByteString.copyFrom(multihash.encoded()))
                .setClusterLevelRaw(0);

        Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                .setId(ByteString.copyFrom(server.self().encoded()));
        for (Multiaddr multiaddr : server.publishMultiaddrs()) {
            peerBuilder.addAddrs(ByteString.copyFrom(multiaddr.address()));
        }
        builder.addProviderPeers(peerBuilder.build());

        return builder.build();
    }


    static Set<PeerId> getProvider(Connection connection, Cid cid) {

        Set<PeerId> peerIds = new HashSet<>();

        try {
            Dht.Message getProvidersMessage = createGetProvidersMessage(cid.multihash());
            Dht.Message msg = request(connection, getProvidersMessage);


            List<Dht.Message.Peer> list = msg.getProviderPeersList();
            for (Dht.Message.Peer entry : list) {
                PeerId peerId = PeerId.create(entry.getId().toByteArray());
                peerIds.add(peerId);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable.getClass().getSimpleName());
        }

        return peerIds;

    }


    static void provide(Server server, Connection connection, Cid cid)
            throws ExecutionException, InterruptedException, TimeoutException {
        message(connection, createAddProviderMessage(server, cid.multihash()));
    }

    static boolean putValue(Connection connection, RecordOuterClass.Record record) {

        Dht.Message putValueMessage = createPutValueMessage(record);
        try {
            Dht.Message msg = request(connection, putValueMessage);
            return msg.hasRecord();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable.getClass().getSimpleName());
        }

        return false;
    }

    static void message(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ExecutionException {

        CompletableFuture<Void> done = new CompletableFuture<>();

        StreamRequester.createStream(connection, new DhtMessage(done))
                .request(DataHandler.encode(message, IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.DHT_PROTOCOL), IPFS.DHT_MESSAGE_TIMEOUT);
        done.get(IPFS.DHT_MESSAGE_TIMEOUT, TimeUnit.SECONDS);

    }

    static Dht.Message request(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ExecutionException {

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();

        StreamRequester.createStream(connection, new DhtRequest(done))
                .request(DataHandler.encode(message, IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.DHT_PROTOCOL), IPFS.DHT_REQUEST_TIMEOUT);

        return done.get(IPFS.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

    }

    record DhtMessage(CompletableFuture<Void> done) implements StreamRequester {
        private static final String TAG = DhtMessage.class.getSimpleName();

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                    IPFS.DHT_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
            if (Objects.equals(protocol, IPFS.DHT_PROTOCOL)) {
                done.complete(null); // this is not 100% correct [a fin would be much nicer]
                stream.closeInput(TransportError.Code.NO_ERROR);
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            throw new Exception("data method invoked [not expected]");
        }

        @Override
        public void fin(Stream stream) {
            // this is shit, it seems never invoked (otherwise I can react when message is done)
            LogUtils.error(TAG, "fin received " + stream.connection().remotePeerId());
        }
    }

    record DhtRequest(CompletableFuture<Dht.Message> done) implements StreamRequester {

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                    IPFS.DHT_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(Dht.Message.parseFrom(data));
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
        }

    }
}
