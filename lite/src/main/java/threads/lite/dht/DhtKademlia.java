package threads.lite.dht;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import dht.pb.Dht;
import record.pb.RecordOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Host;
import threads.lite.core.Page;
import threads.lite.core.Resolver;
import threads.lite.core.Routing;
import threads.lite.core.Server;
import threads.lite.ipns.IpnsService;
import threads.lite.quic.Connection;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.quic.Parameters;
import threads.lite.quic.StreamRequester;


public final class DhtKademlia implements Routing, AutoCloseable {

    private static final String TAG = DhtKademlia.class.getSimpleName();

    @NonNull
    private final RoutingTable routingTable;
    @NonNull
    private final Host host;
    @NonNull
    private final ReentrantLock lock = new ReentrantLock();
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public DhtKademlia(@NonNull Host host) {
        this.host = host;
        this.routingTable = new RoutingTable(ConcurrentHashMap.newKeySet());
    }

    private static void processValues(@Nullable Page best,
                                      @NonNull Page current,
                                      @NonNull Consumer<Page> reporter) {

        if (best != null) {
            int value = IpnsService.compare(best, current);
            if (value == -1) { // "current" is newer entry
                reporter.accept(current);
            }
        } else {
            reporter.accept(current);
        }
    }

    private void bootstrap() {
        lock.lock();
        try {
            if (routingTable.isEmpty()) {
                Set<Peer> peers = host.getRoutingPeers();
                for (Peer peer : peers) {
                    routingTable.addPeer(peer);
                }
                List<Peer> randomPeers = host.getPeerStore().getRandomPeers(IPFS.DHT_TABLE_SIZE);

                for (Peer peer : randomPeers) {
                    if (peer.multiaddr().protocolSupported(host.ipv().get())) {
                        routingTable.addPeer(peer);
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    private List<Peer> evalClosestPeers(@NonNull Dht.Message pms) {

        List<Peer> peers = new ArrayList<>();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();

        for (Dht.Message.Peer entry : list) {

            PeerId peerId = PeerId.create(entry.getId().toByteArray());

            Multiaddr resolved = Resolver.reduceToOne(host.ipv(), peerId, entry.getAddrsList());
            if (resolved != null) {
                try {
                    peers.add(Peer.create(resolved, true));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

        }
        return peers;
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer,
                                 @NonNull PeerId peerId) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();
            Set<Peer> handled = ConcurrentHashMap.newKeySet();
            byte[] target = peerId.encoded();
            ID key = ID.convertPeerID(peerId);

            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(target);
            getClosestPeers(done, key, findNodeMessage, peers -> {
                for (Peer peer : peers) {
                    if (handled.add(peer)) {
                        consumer.accept(peer.multiaddr());
                    }
                }
            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void getClosestPeers(@NonNull Cancellable cancellable, @NonNull ID key,
                                 @NonNull Dht.Message message, @NonNull Consumer<List<Peer>> channel)
            throws InterruptedException {

        runQuery(cancellable, key, (ctx1, peer) -> {

            Dht.Message pms = request(ctx1, peer, message);

            List<Peer> peers = evalClosestPeers(pms);

            channel.accept(peers);

            return peers;
        });

    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull RecordOuterClass.Record record) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            // don't allow local users to put bad values.
            Page entry = IpnsService.validate(IPFS.DHT_PROTOCOL, record);
            Objects.requireNonNull(entry);

            Dht.Message putValueMessage = DhtService.createPutValueMessage(record);
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(
                    record.getKey().toByteArray());

            Set<Peer> handled = ConcurrentHashMap.newKeySet();
            ID key = ID.convertKey(record.getKey().toByteArray());
            getClosestPeers(done, key, findNodeMessage, peers -> {

                for (Peer peer : peers) {
                    if (handled.add(peer)) {
                        try {
                            Dht.Message res = request(cancellable, peer, putValueMessage);
                            Objects.requireNonNull(res);

                            if (res.hasRecord()) {
                                // no extra validation, should be fine
                                consumer.accept(peer.multiaddr());
                            }

                        } catch (Throwable ignore) {
                        }
                    }
                }
            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer,
                              @NonNull Cid cid) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            Dht.Message message = DhtService.createGetProvidersMessage(cid.multihash());

            byte[] multihash = cid.multihash().encoded();
            ID key = ID.convertKey(multihash);

            Set<PeerId> handled = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, p) -> {

                Dht.Message pms = request(ctx, p, message);

                List<Dht.Message.Peer> list = pms.getProviderPeersList();

                for (Dht.Message.Peer entry : list) {
                    if (entry.getAddrsCount() > 0) {
                        PeerId peerId = PeerId.create(entry.getId().toByteArray());
                        // contains circuit addresses
                        Multiaddr multiaddr = Resolver.reduceToOne(host.ipv(),
                                peerId, entry.getAddrsList());
                        if (multiaddr != null) {
                            if (handled.add(multiaddr.peerId())) {
                                consumer.accept(multiaddr);
                            }
                        }
                    }
                }

                return evalClosestPeers(pms);

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    void addToRouting(@NonNull QueryPeer queryPeer) {

        Peer peer = queryPeer.peer();
        boolean added = routingTable.addPeer(peer);


        // only replaceable peer are adding
        // (the replaceable peers are initial the routing peers)
        if (added && peer.replaceable()) {
            host.getPeerStore().storePeer(peer);
        }
    }


    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Server server,
                        @NonNull Consumer<Multiaddr> consumer,
                        @NonNull Cid cid) {


        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            byte[] multihash = cid.multihash().encoded();
            ID key = ID.convertKey(multihash);
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(multihash);
            Dht.Message message = DhtService.createAddProviderMessage(server, cid.multihash());

            Set<Peer> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(done, key, findNodeMessage, peers -> {

                for (Peer peer : peers) {
                    if (handled.add(peer)) {

                        Connection connection = null;
                        try {
                            if (cancellable.isCancelled()) {
                                throw new InterruptedException();
                            }
                            connection = ConnectionBuilder.connect(host.getLiteResponder(),
                                    peer.multiaddr(), Parameters.getDefault(),
                                    host.getLiteCertificate(), host.ipv());

                            DhtService.message(connection, message);

                            // success assumed
                            consumer.accept(StreamRequester.remoteMultiaddr(connection));

                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable.getClass().getSimpleName());
                        } finally {
                            if (connection != null) {
                                connection.close();
                            }
                        }

                    }
                }

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception here
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @NonNull
    private Dht.Message request(@NonNull Cancellable cancellable, @NonNull Peer peer,
                                @NonNull Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {


        if (cancellable.isCancelled()) {
            throw new InterruptedException();
        }

        Connection connection = null;
        try {
            connection = ConnectionBuilder.connect(host.getLiteResponder(),
                    peer.multiaddr(), Parameters.getDefault(),
                    host.getLiteCertificate(), host.ipv());

            return DhtService.request(connection, message);
        } catch (ExecutionException exception) {
            throw new ConnectException(exception.getClass().getSimpleName());
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId id) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            byte[] target = id.encoded();
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(target);
            ID key = ID.convertKey(target);

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, peer) -> {

                Dht.Message pms = request(ctx, peer, findNodeMessage);

                List<Peer> peers = new ArrayList<>();
                List<Dht.Message.Peer> list = pms.getCloserPeersList();
                for (Dht.Message.Peer entry : list) {

                    PeerId peerId = PeerId.create(entry.getId().toByteArray());
                    if (Objects.equals(peerId, id)) {
                        // contains circuit addresses
                        Multiaddrs multiaddrs = Resolver.reduce(host.ipv(),
                                peerId, entry.getAddrsList());
                        for (Multiaddr multiaddr : multiaddrs) {
                            if (handled.add(multiaddr)) {
                                consumer.accept(multiaddr);
                            }
                        }
                    } else {
                        Multiaddr resolved = Resolver.reduceToOne(host.ipv(),
                                peerId, entry.getAddrsList());
                        if (resolved != null) {
                            try {
                                peers.add(Peer.create(resolved, true));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    }
                }
                return peers;

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void runQuery(@NonNull Cancellable cancellable, @NonNull ID key,
                          @NonNull QueryFunc queryFn) throws InterruptedException {

        // pick the K closest peers to the key in our Routing table.
        List<QueryPeer> seedPeers = routingTable.nearestPeers(key);
        Query.runQuery(this, cancellable, key, seedPeers, queryFn);

    }

    private List<Peer> getRecordOfPeers(@NonNull Cancellable cancellable,
                                        @NonNull Peer peer,
                                        @NonNull Consumer<Page> consumer,
                                        @NonNull Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {


        Dht.Message pms = request(cancellable, peer, message);

        List<Peer> peers = evalClosestPeers(pms);

        if (pms.hasRecord()) {

            RecordOuterClass.Record record = pms.getRecord();
            try {
                Page entry = IpnsService.validate(IPFS.DHT_PROTOCOL, record);
                consumer.accept(entry);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage());
            }
        }

        return peers;
    }

    void removeFromRouting(QueryPeer peer, boolean removeBootstrap) {
        boolean result = routingTable.removePeer(peer);
        if (result) {
            LogUtils.info(TAG, "Remove from routing " + peer);
        }
        if (removeBootstrap) {
            // remove from bootstrap
            host.getPeerStore().removePeer(peer.peer());
        }
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<Page> consumer,
                            @NonNull byte[] target) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();
            Dht.Message message = DhtService.createGetValueMessage(target);
            AtomicReference<Page> best = new AtomicReference<>();
            ID key = ID.convertKey(target);

            runQuery(done, key, (ctx1, peer) -> getRecordOfPeers(ctx1, peer,
                    entry -> processValues(best.get(), entry, (current) -> {
                        consumer.accept(current);
                        best.set(current);
                    }), message));
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void close() {
        routingTable.clear();
        closed.set(true);
    }

    private boolean isClosed() {
        return closed.get();
    }


    public interface QueryFunc {
        @NonNull
        List<Peer> query(@NonNull Cancellable cancellable, @NonNull Peer peer)
                throws InterruptedException, ConnectException, TimeoutException;
    }
}
