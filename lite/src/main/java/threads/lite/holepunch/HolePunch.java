package threads.lite.holepunch;


import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.Objects;
import java.util.concurrent.Executors;

import holepunch.pb.Holepunch;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.Resolver;
import threads.lite.core.Server;
import threads.lite.quic.Stream;

// Documentation https://github.com/libp2p/specs/blob/master/relay/DCUtR.md

// NAT traversal is a quintessential problem in peer-to-peer networks.
//
// We currently utilize relays, which allow us to traverse NATs by using a third party as proxy.
// Relays are a reliable fallback, that can connect peers behind NAT albeit with a high-latency,
// low-bandwidth connection. Unfortunately, they are expensive to scale and maintain if they have
// to carry all the NATed node traffic in the network.
//
// It is often possible for two peers behind NAT to communicate directly by utilizing a
// technique called hole punching[1]. The technique relies on the two peers synchronizing
// and simultaneously opening connections to each other to their predicted external address.
// It works well for UDP, and reasonably well for TCP.
//
// The problem in hole punching, apart from not working all the time, is the need for rendezvous
// and synchronization. This is usually accomplished using dedicated signaling servers [2]. However,
// this introduces yet another piece of infrastructure, while still requiring the use of relays
// as a fallback for the cases where a direct connection is not possible.
//
// In this specification, we describe a synchronization protocol for direct connectivity with
// hole punching that eschews signaling servers and utilizes existing relay connections instead.
// That is, peers start with a relay connection and synchronize directly, without the use of a
// signaling server. If the hole punching attempt is successful, the peers upgrade their
// connection to a direct connection and they can close the relay connection. If the hole punching
// attempt fails, they can keep using the relay connection as they were.
//
// Consider two peers, A and B. A wants to connect to B, which is behind a NAT and advertises
// relay addresses. A may itself be behind a NAT or be a public node.
//
// The protocol starts with the completion of a relay connection from A to B. Upon observing
// the new connection, the inbound peer (here B) checks the addresses advertised by A via identify.
// If that set includes public addresses, then A may be reachable by a direct connection,
// in which case B attempts a unilateral connection upgrade by initiating a direct connection to A.
//
// If the unilateral connection upgrade attempt fails or if A is itself a NATed peer that doesn't
// advertise public address, then B initiates the direct connection upgrade protocol as follows:
// (1) B opens a stream to A using the /libp2p/dcutr protocol. (->this has done before any of
// the function has to
//
// [here it starts the implementation see initialize]
//
public interface HolePunch {
    String TAG = HolePunch.class.getSimpleName();
    String TIMER = "TIMER";
    String ADDRS = "ADDRS";

    // initializeConnect is invoked by [B SERVER]
    static void initializeConnect(Stream stream, Multiaddr observed) {
        // (2) B sends to A a Connect message containing its observed (and possibly predicted)
        // addresses from identify and starts a timer to measure RTT of the relay connection.
        Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                .setType(Holepunch.HolePunch.Type.CONNECT);

        // Note: observed is B's observed address
        LogUtils.error(TAG, "[B SERVER] Observed Address " + observed);
        builder.addObsAddrs(ByteString.copyFrom(observed.address()));

        Holepunch.HolePunch message = builder.build();
        stream.setAttribute(TIMER, System.currentTimeMillis());
        stream.writeOutput(DataHandler.encode(message), false);

    }

    // response is invoked by [A]
    @Nullable
    static Multiaddr response(HolePunchInfo holePunchInfo, Stream stream, byte[] data)
            throws Exception {

        Holepunch.HolePunch holePunch = Holepunch.HolePunch.parseFrom(data);

        if (holePunch.getType() == Holepunch.HolePunch.Type.CONNECT) {

            // Upon receiving the Connect, A responds back with a Connect message containing
            // its observed (and possibly predicted) addresses.

            // peerId is from B
            LogUtils.error(TAG, "[A] PeerId [B] " + holePunchInfo.getPeerId());

            // circuit addresses are sorted out here
            Multiaddrs multiaddrs = Multiaddr.create(holePunchInfo.getPeerId(),
                    holePunch.getObsAddrsList(), holePunchInfo.ipv().get());

            LogUtils.error(TAG, "[A] PeerId [B] Multiaddrs " + multiaddrs);

            if (multiaddrs.size() == 0) {
                LogUtils.error(TAG, "[B] Empty Observed Multiaddrs from [A], abort");
                throw new Exception("Empty Observed Multiaddrs");
            }

            stream.setAttribute(ADDRS, multiaddrs);

            LogUtils.error(TAG, "[A] Send Observed Address " + holePunchInfo.observed());

            Holepunch.HolePunch.Builder builder =
                    Holepunch.HolePunch.newBuilder()
                            .setType(Holepunch.HolePunch.Type.CONNECT);

            builder.addObsAddrs(ByteString.copyFrom(holePunchInfo.observed().address()));
            stream.writeOutput(DataHandler.encode(builder.build()), false);
            return null;
        } else if (holePunch.getType() == Holepunch.HolePunch.Type.SYNC) {
            // Upon receiving the Sync, A immediately dials the address to B.
            Multiaddrs multiaddrs = (Multiaddrs) stream.getAttribute(ADDRS);
            Objects.requireNonNull(multiaddrs, "No Multiaddrs"); // should not happen


            Multiaddr multiaddr = Resolver.reduceToOne(multiaddrs);

            LogUtils.error(TAG, "[A] Connect Addresses [B] " + multiaddrs);

            Objects.requireNonNull(multiaddr);

            return multiaddr;
        } else {
            throw new Exception("not expected hole punch type");
        }
    }

    // sendSync is is invoked by [B]
    static void sendSync(Server server, Stream stream, PeerId peerId,
                         byte[] data) throws Exception {
        // (4) Upon receiving the Connect, B sends a Sync message and starts a timer for half the
        // RTT measured from the time between sending the initial Connect and receiving the
        // response. The purpose of the Sync message and B's timer is to allow the two peers
        // to synchronize so that they perform a simultaneous open that allows hole
        // punching to succeed.

        // note peerId is user [A]
        LogUtils.error(TAG, "[B SERVER] PeerId [A] " + peerId);

        Long timer = (Long) stream.getAttribute(TIMER); // timer set earlier
        Objects.requireNonNull(timer, "Timer not set on stream");
        long rtt = System.currentTimeMillis() - timer;

        // B receives the Connect message from A
        Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data);

        LogUtils.error(TAG, "[B SERVER] Request took " + rtt);
        Objects.requireNonNull(msg);


        if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
            throw new Exception("[A] send wrong message connect type, abort");
        }

        // circuit addresses are sorted out here
        Multiaddrs multiaddrs = Multiaddr.create(peerId, msg.getObsAddrsList(),
                server.ipv().get());

        if (multiaddrs.size() == 0) {
            throw new Exception("[A] send no observed addresses, abort");
        }

        // now B sends a Sync message to A
        msg = Holepunch.HolePunch.newBuilder().
                setType(Holepunch.HolePunch.Type.SYNC).build();
        stream.writeOutput(DataHandler.encode(msg), false);


        // Next, B wait for rtt/2
        Thread.sleep(rtt / 2);

        LogUtils.error(TAG, "[B SERVER] Hole Punch Addresses for [A] " + multiaddrs);

        // (5) Simultaneous Connect. The two nodes follow the steps below in parallel for every
        // address obtained from the Connect message

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.

        // TODO [Future urgent] replace by virtual threads
        Executors.newSingleThreadExecutor().execute(() -> server.holePunching(multiaddrs));

    }

}
