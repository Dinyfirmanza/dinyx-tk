package threads.lite.store;

import androidx.room.RoomDatabase;

import threads.lite.cid.Multiaddr;

@androidx.room.Database(entities = {Multiaddr.class}, version = 4, exportSchema = false)
public abstract class SwarmDatabase extends RoomDatabase {

    public abstract SwarmDao swarmDao();

}
