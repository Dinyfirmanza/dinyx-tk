package threads.lite.store;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.Date;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Page;
import threads.lite.core.PageStore;


public final class PAGES implements PageStore {

    private static volatile PAGES INSTANCE = null;

    private final PageDatabase pageDatabase;

    private PAGES(PageDatabase pageDatabase) {
        this.pageDatabase = pageDatabase;
    }

    public static PAGES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PAGES.class) {
                if (INSTANCE == null) {
                    PageDatabase pageDatabase = Room.databaseBuilder(context,
                                    PageDatabase.class,
                                    PageDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new PAGES(pageDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void storePage(@NonNull Page page) {
        pageDatabase.pageDao().insertPage(page);
    }

    @Override
    @Nullable
    public Page getPage(@NonNull PeerId peerId) {
        return pageDatabase.pageDao().getPage(peerId);
    }


    @Override
    public void updatePageContent(@NonNull PeerId peerId, @NonNull Cid cid, @NonNull Date eol) {
        pageDatabase.pageDao().update(peerId, Page.encodeIpnsData(cid), eol.getTime());
    }

    @Override
    public void clear() {
        pageDatabase.clearAllTables();
    }

    @Override
    @Nullable
    public Cid getPageContent(@NonNull PeerId peerId) {
        try {
            byte[] data = pageDatabase.pageDao().getValue(peerId);
            if (data != null) {
                return Page.decodeIpnsData(data);
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
        return null;
    }

}
