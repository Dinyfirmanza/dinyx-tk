package threads.lite.tls;

import android.util.SparseArray;

import androidx.annotation.Nullable;

public enum HandshakeType {
    client_hello(1),
    server_hello(2),
    new_session_ticket(4),
    end_of_early_data(5),
    encrypted_extensions(8),
    certificate(11),
    certificate_request(13),
    certificate_verify(15),
    finished(20),
    key_update(24),
    message_hash(254),
    ;

    private static final SparseArray<HandshakeType> byValue = new SparseArray<>();

    static {
        for (HandshakeType t : HandshakeType.values()) {
            byValue.put(t.value, t);
        }
    }

    public final byte value;

    HandshakeType(int value) {
        this.value = (byte) value;
    }

    @Nullable
    public static HandshakeType get(int value) {
        return byValue.get(value);
    }
}
