package threads.lite.quic;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


final class ConnectionDestinationIdRegistry extends ConnectionIdRegistry {

    private volatile int notRetiredThreshold;  // all sequence numbers below are retired


    ConnectionDestinationIdRegistry(byte[] initialConnectionId) {
        super(new ConnectionIdInfo(0, initialConnectionId,
                null, ConnectionIdStatus.IN_USE));
    }

    /**
     * @return whether the connection id could be added as new; when its sequence number implies that it as retired already, false is returned.
     */
    boolean registerNewConnectionId(int sequenceNr, byte[] connectionId, byte[] statelessResetToken) {

        if (sequenceNr >= notRetiredThreshold) {
            connectionIds().add(new ConnectionIdInfo(sequenceNr, connectionId, statelessResetToken,
                    ConnectionIdStatus.NEW));
            return true;
        } else {
            connectionIds().add(new ConnectionIdInfo(sequenceNr, connectionId, statelessResetToken,
                    ConnectionIdStatus.RETIRED));
            return false;
        }
    }

    List<Integer> retireAllBefore(int retirePriorTo) {
        notRetiredThreshold = retirePriorTo;

        List<Integer> toRetire = new ArrayList<>();
        for (ConnectionIdInfo connectionIdInfo : connectionIds()) {
            int sequenceNumber = connectionIdInfo.getSequenceNumber();
            if (sequenceNumber < retirePriorTo) {
                if (connectionIdInfo.getConnectionIdStatus() != ConnectionIdStatus.RETIRED) {
                    connectionIdInfo.setStatus(ConnectionIdStatus.RETIRED);
                    toRetire.add(sequenceNumber);
                }
            }
        }


        // Find one that is not retired
        ConnectionIdInfo nextCid = connectionIds().stream()
                .filter(cid -> cid.getConnectionIdStatus() != ConnectionIdStatus.RETIRED)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Can't find connection id that is not retired"));
        nextCid.setStatus(ConnectionIdStatus.IN_USE);


        return toRetire;
    }


    /**
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-detecting-a-stateless-reset">...</a>
     * "... but excludes stateless reset tokens associated with connection IDs that are either unused or retired."
     */
    boolean isStatelessResetToken(byte[] tokenCandidate) {
        return connectionIds().stream()
                .filter(cid -> cid.getConnectionIdStatus().notUnusedOrRetired())
                .anyMatch(cid -> Arrays.equals(cid.getStatelessResetToken(), tokenCandidate));
    }
}

