package threads.lite.quic;


import java.util.concurrent.atomic.AtomicReference;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import threads.lite.LogUtils;
import threads.lite.tls.CipherSuite;
import threads.lite.tls.TrafficSecrets;

class ConnectionSecrets {
    private static final String TAG = ConnectionSecrets.class.getSimpleName();

    // https://www.rfc-editor.org/rfc/rfc9001.html#name-initial-secrets
    // "initial_salt = 0x38762cf7f55934b34d179ae6a4c80cadccbb7f0a"
    private static final byte[] STATIC_SALT_V1 = new byte[]{
            (byte) 0x38, (byte) 0x76, (byte) 0x2c, (byte) 0xf7, (byte) 0xf5, (byte) 0x59, (byte) 0x34, (byte) 0xb3,
            (byte) 0x4d, (byte) 0x17, (byte) 0x9a, (byte) 0xe6, (byte) 0xa4, (byte) 0xc8, (byte) 0x0c, (byte) 0xad,
            (byte) 0xcc, (byte) 0xbb, (byte) 0x7f, (byte) 0x0a};
    // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-initial-salt
    // "The salt used to derive Initial keys in Section 5.2 of [QUIC-TLS] changes to:
    //  initial_salt = 0xa707c203a59b47184a1d62ca570406ea7ae3e5d3"
    private static final byte[] STATIC_SALT_V2 = new byte[]{
            (byte) 0xa7, (byte) 0x07, (byte) 0xc2, (byte) 0x03, (byte) 0xa5, (byte) 0x9b, (byte) 0x47, (byte) 0x18,
            (byte) 0x4a, (byte) 0x1d, (byte) 0x62, (byte) 0xca, (byte) 0x57, (byte) 0x04, (byte) 0x06, (byte) 0xea,
            (byte) 0x7a, (byte) 0xe3, (byte) 0xe5, (byte) 0xd3};


    protected final Version version;
    protected final Role role;

    private final AtomicReference<Keys> clientSecretsInitial = new AtomicReference<>(null);
    private final AtomicReference<Keys> serverSecretsInitial = new AtomicReference<>(null);

    private final AtomicReference<Keys> clientSecretsHandshake = new AtomicReference<>(null);
    private final AtomicReference<Keys> serverSecretsHandshake = new AtomicReference<>(null);

    private final AtomicReference<Keys> clientSecretsApp = new AtomicReference<>(null);
    private final AtomicReference<Keys> serverSecretsApp = new AtomicReference<>(null);


    public ConnectionSecrets(Version version, Role role) {
        this.version = version;
        this.role = role;
    }


    static byte[] hmacSHA256(byte[] initialSalt, byte[] inputKeyingMaterial) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");

            SecretKey salt = new SecretKeySpec(initialSalt, "HmacSHA256");

            if (inputKeyingMaterial == null || inputKeyingMaterial.length == 0) {
                throw new IllegalArgumentException("provided inputKeyingMaterial must be at least of size 1 and not null");
            }
            mac.init(salt);

            return mac.doFinal(inputKeyingMaterial);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new IllegalStateException(throwable);
        }
    }

    private static byte[] computeInitialSecret(Version actualVersion, byte[] destConnectionId) {
        // https://www.rfc-editor.org/rfc/rfc9001.html#name-initial-secrets
        // "The hash function for HKDF when deriving initial secrets and keys is SHA-256"

        byte[] initialSalt = actualVersion.isV2() ? STATIC_SALT_V2 : STATIC_SALT_V1;
        return hmacSHA256(initialSalt, destConnectionId);
    }

    /**
     * Generate the initial secrets
     */
    final void computeInitialKeys(byte[] destConnectionId) {

        byte[] initialSecret = computeInitialSecret(version, destConnectionId);

        clientSecretsInitial.set(Keys.createInitialKeys(version, initialSecret, Role.Client));
        serverSecretsInitial.set(Keys.createInitialKeys(version, initialSecret, Role.Server));
    }

    final void createKeys(Level level, TrafficSecrets secrets, CipherSuite selectedCipherSuite) {

        if (selectedCipherSuite != CipherSuite.TLS_AES_128_GCM_SHA256) {
            throw new IllegalStateException("unsupported cipher suite " + selectedCipherSuite);
        }

        switch (level) {
            case Handshake -> {
                this.clientSecretsHandshake.set(
                        Keys.computeHandshakeKeys(version, Role.Client, secrets));
                this.serverSecretsHandshake.set(
                        Keys.computeHandshakeKeys(version, Role.Server, secrets));
            }
            case App -> {
                this.clientSecretsApp.set(
                        Keys.computeApplicationKeys(version, Role.Client, secrets));
                this.serverSecretsApp.set(
                        Keys.computeApplicationKeys(version, Role.Server, secrets));
            }
            default -> throw new RuntimeException("not supported level");
        }

    }

    final void computeHandshakeSecrets(TrafficSecrets secrets, CipherSuite selectedCipherSuite) {
        createKeys(Level.Handshake, secrets, selectedCipherSuite);
    }

    final void computeApplicationSecrets(TrafficSecrets secrets, CipherSuite selectedCipherSuite) {
        createKeys(Level.App, secrets, selectedCipherSuite);
    }

    final Keys remoteSecrets(Level level) {
        if (role == Role.Client) {
            return switch (level) {
                case Initial -> serverSecretsInitial.get();
                case App -> serverSecretsApp.get();
                case Handshake -> serverSecretsHandshake.get();
            };
        } else {
            return switch (level) {
                case Initial -> clientSecretsInitial.get();
                case App -> clientSecretsApp.get();
                case Handshake -> clientSecretsHandshake.get();
            };
        }
    }

    final void remoteSecrets(Level level, Keys keys) {
        if (role == Role.Client) {
            switch (level) {
                case Initial -> serverSecretsInitial.set(keys);
                case App -> serverSecretsApp.set(keys);
                case Handshake -> serverSecretsHandshake.set(keys);
            }
        } else {
            switch (level) {
                case Initial -> clientSecretsInitial.set(keys);
                case App -> clientSecretsApp.set(keys);
                case Handshake -> clientSecretsHandshake.set(keys);
            }
        }
    }


    final void ownSecrets(Level level, Keys keys) {
        if (role == Role.Client) {
            switch (level) {
                case Initial -> clientSecretsInitial.set(keys);
                case App -> clientSecretsApp.set(keys);
                case Handshake -> clientSecretsHandshake.set(keys);
            }

        } else {
            switch (level) {
                case Initial -> serverSecretsInitial.set(keys);
                case App -> serverSecretsApp.set(keys);
                case Handshake -> serverSecretsHandshake.set(keys);
            }
        }
    }


    final Keys ownSecrets(Level level) {
        if (role == Role.Client) {
            return switch (level) {
                case Initial -> clientSecretsInitial.get();
                case App -> clientSecretsApp.get();
                case Handshake -> clientSecretsHandshake.get();
            };

        } else {
            return switch (level) {
                case Initial -> serverSecretsInitial.get();
                case App -> serverSecretsApp.get();
                case Handshake -> serverSecretsHandshake.get();
            };
        }
    }


    final void discardHandshakeKeys() {
        clientSecretsHandshake.set(null);
        serverSecretsHandshake.set(null);
    }


    final void discardInitialKeys() {
        clientSecretsInitial.set(null);
        serverSecretsInitial.set(null);
    }

    final void discardKeys() {
        clientSecretsHandshake.set(null);
        serverSecretsHandshake.set(null);
        clientSecretsInitial.set(null);
        serverSecretsInitial.set(null);
        clientSecretsApp.set(null);
        serverSecretsApp.set(null);
    }
}
