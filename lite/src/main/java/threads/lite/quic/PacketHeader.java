package threads.lite.quic;


record PacketHeader(Level level, Version version, byte flags, int posFlags,
                    byte[] destinationConnectionId, byte[] sourceConnectionId,
                    byte[] token) {
}
