package threads.lite.dag;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Dir;
import threads.lite.core.Cancellable;
import threads.lite.core.Link;
import threads.lite.core.ReaderInputStream;
import threads.lite.core.Session;
import unixfs.pb.Unixfs;

public interface DagService {

    @NonNull
    static Merkledag.PBNode createRaw(byte[] bytes) {
        Unixfs.Data.Builder data = Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                .setFilesize(bytes.length)
                .setData(ByteString.copyFrom(bytes));

        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));

        return pbn.build();
    }

    static DirectoryNode createDirectory(@NonNull List<Link> links) {
        Unixfs.Data.Builder builder = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory);
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        long fileSize = 0;
        for (Link link : links) {
            long size = link.size();
            fileSize = fileSize + size;
            builder.addBlocksizes(size);

            Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                    .setName(link.name())
                    .setTsize(link.size());

            lnb.setHash(ByteString.copyFrom(link.cid().encoded()));

            pbn.addLinks(lnb.build());
        }
        builder.setFilesize(fileSize);
        byte[] unixData = builder.build().toByteArray();

        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), fileSize);

    }

    static DirectoryNode createDirectory() {
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .build().toByteArray();
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), 0);
    }

    static boolean isDirectory(@NonNull Merkledag.PBNode node) throws Exception {
        Unixfs.Data unixData = DagReader.getData(node);

        return unixData.getType() == unixfs.pb.Unixfs.Data.DataType.Directory;
    }

    @NonNull
    private static Block getBlock(Session session, Cancellable cancellable, Cid cid) throws Exception {
        Block block = session.getBlockStore().getBlock(cid);
        if (block != null) {
            return block;
        }
        return session.getBlock(cancellable, cid);
    }

    @NonNull
    static Merkledag.PBNode getNode(@NonNull Session session,
                                    @NonNull Cancellable cancellable,
                                    @NonNull Cid cid) throws Exception {
        Block block = getBlock(session, cancellable, cid);
        return block.node();
    }

    static Dir createDirectory(@NonNull Session session, @NonNull List<Link> links) throws Exception {
        DagService.DirectoryNode directory = DagService.createDirectory(links);
        Block block = Block.createBlock(directory.node());
        session.getBlockStore().storeBlock(block);
        return new Dir(block.cid(), directory.size());
    }

    static Dir createEmptyDirectory(@NonNull Session session) throws Exception {
        DagService.DirectoryNode directory = DagService.createDirectory();
        Block block = Block.createBlock(directory.node());
        session.getBlockStore().storeBlock(block);
        return new Dir(block.cid(), directory.size());
    }

    @NonNull
    static Dir addChild(@NonNull Session session,
                        @NonNull Merkledag.PBNode dirNode,
                        @NonNull Link link) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long dirSize = unixData.getFilesize();

        long size = link.size();
        Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                .setName(link.name())
                .setHash(ByteString.copyFrom(link.cid().encoded()))
                .setTsize(size);

        long newDirSize = dirSize + size;

        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.setFilesize(newDirSize);
        builder.addBlocksizes(size);

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        pbn.addLinks(lnb.build());
        pbn.setData(ByteString.copyFrom(builder.build().toByteArray()));

        Block block = Block.createBlock(pbn.build());
        session.getBlockStore().storeBlock(block);

        return new Dir(block.cid(), newDirSize);
    }

    @NonNull
    static Dir updateChild(@NonNull Session session,
                           @NonNull Merkledag.PBNode dirNode,
                           @NonNull Link link) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink pbLink = dirNode.getLinks(index);
            if (Objects.equals(pbLink.getName(), link.name())) {
                pbn.removeLinks(index); // remove the update link when available
            } else {
                filesize = filesize + pbLink.getTsize();
                builder.addBlocksizes(pbLink.getTsize());
            }
        }

        // added the updated link
        Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                .setName(link.name())
                .setHash(ByteString.copyFrom(link.cid().encoded()))
                .setTsize(link.size());
        pbn.addLinks(lnb);


        filesize = filesize + lnb.getTsize();
        builder.addBlocksizes(lnb.getTsize());

        builder.setFilesize(filesize);
        pbn.setData(ByteString.copyFrom(builder.build().toByteArray()));
        Block block = Block.createBlock(pbn.build());
        session.getBlockStore().storeBlock(block);

        return new Dir(block.cid(), filesize);
    }

    static Dir removeChild(@NonNull Session session,
                           @NonNull Merkledag.PBNode dirNode,
                           @NonNull String name) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink link = dirNode.getLinks(index);
            if (Objects.equals(link.getName(), name)) {
                pbn.removeLinks(index);
            } else {
                filesize = filesize + link.getTsize();
                builder.addBlocksizes(link.getTsize());
            }
        }

        builder.setFilesize(filesize);
        pbn.setData(ByteString.copyFrom(builder.build().toByteArray()));
        Block block = Block.createBlock(pbn.build());
        session.getBlockStore().storeBlock(block);

        return new Dir(block.cid(), filesize);

    }

    @NonNull
    static Cid createFromStream(@NonNull Session session,
                                @NonNull ReaderInputStream reader) throws Exception {
        return DagWriter.trickle(reader, session.getBlockStore());
    }

    record DirectoryNode(Merkledag.PBNode node, long size) {
    }
}
