package threads.lite.cert;

/**
 * A basic parser for a SEQUENCE object
 */
interface ASN1SequenceParser
        extends ASN1Encodable, InMemoryRepresentable {

}
