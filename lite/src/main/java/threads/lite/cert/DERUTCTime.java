package threads.lite.cert;

/**
 * DER UTC time object.
 */
public final class DERUTCTime extends ASN1UTCTime {

    public DERUTCTime(String time) {
        super(time);
    }

}
