package threads.lite.cert;

import java.io.IOException;

abstract class ASN1UniversalType extends ASN1Type {

    ASN1UniversalType(Class<? extends ASN1Object> javaClass) {
        super(javaClass);
    }

    final ASN1Primitive checkedCast(ASN1Primitive primitive) {
        if (javaClass.isInstance(primitive)) {
            return primitive;
        }

        throw new IllegalStateException("unexpected object: " + primitive.getClass().getName());
    }

    final ASN1Primitive fromByteArray(byte[] bytes) throws IOException {
        return checkedCast(ASN1Primitive.fromByteArray(bytes));
    }

    final ASN1Primitive getContextInstance(ASN1TaggedObject taggedObject, boolean declaredExplicit) {
        if (BERTags.CONTEXT_SPECIFIC != taggedObject.getTagClass()) {
            throw new IllegalStateException("this method only valid for CONTEXT_SPECIFIC tags");
        }

        return checkedCast(taggedObject.getBaseUniversal(declaredExplicit, this));
    }

}
