package threads.lite.cert;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Stream that outputs encoding based on definite length.
 */
class DLOutputStream
        extends ASN1OutputStream {
    DLOutputStream(OutputStream os) {
        super(os);
    }

    DLOutputStream getDLSubStream() {
        return this;
    }

    void writeElements(ASN1Encodable[] elements)
            throws IOException {
        for (ASN1Encodable element : elements) {
            element.toASN1Primitive().toDLObject().encode(this, true);
        }
    }

    void writePrimitive(ASN1Primitive primitive) throws IOException {
        primitive.toDLObject().encode(this, true);
    }

    void writePrimitives(ASN1Primitive[] primitives) throws IOException {
        for (ASN1Primitive primitive : primitives) {
            primitive.toDLObject().encode(this, true);
        }
    }
}
