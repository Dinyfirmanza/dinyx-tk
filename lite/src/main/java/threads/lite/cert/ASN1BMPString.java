package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * ASN.1 BMPString object encodes BMP (<i>Basic Multilingual Plane</i>) subset
 * (aka UCS-2) of UNICODE (ISO 10646) characters in codepoints 0 to 65535.
 * <p>
 * At ISO-10646:2011 the term "BMP" has been withdrawn, and replaced by
 * term "UCS-2".
 * </p>
 */
public abstract class ASN1BMPString extends ASN1Primitive implements ASN1String {
    private final char[] string;


    ASN1BMPString(char[] string) {
        if (string == null) {
            throw new NullPointerException("'string' cannot be null");
        }

        this.string = string;
    }

    static ASN1BMPString createPrimitive(char[] string) {
        return new DERBMPString(string);
    }

    public final String getString() {
        return new String(string);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1BMPString that)) {
            return false;
        }

        return Arrays.areEqual(this.string, that.string);
    }


    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, string.length * 2);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        int count = string.length;

        out.writeIdentifier(withTag, BERTags.BMP_STRING);
        out.writeDL(count * 2);

        byte[] buf = new byte[8];

        int i = 0, limit = count & -4;
        while (i < limit) {
            char c0 = string[i], c1 = string[i + 1], c2 = string[i + 2], c3 = string[i + 3];
            i += 4;

            buf[0] = (byte) (c0 >> 8);
            buf[1] = (byte) c0;
            buf[2] = (byte) (c1 >> 8);
            buf[3] = (byte) c1;
            buf[4] = (byte) (c2 >> 8);
            buf[5] = (byte) c2;
            buf[6] = (byte) (c3 >> 8);
            buf[7] = (byte) c3;

            out.write(buf, 0, 8);
        }
        if (i < count) {
            int bufPos = 0;
            do {
                char c0 = string[i];
                i += 1;

                buf[bufPos++] = (byte) (c0 >> 8);
                buf[bufPos++] = (byte) c0;
            }
            while (i < count);

            out.write(buf, 0, bufPos);
        }
    }
}
