package threads.lite.cert;

import java.io.IOException;

/**
 * The DLSequence encodes a SEQUENCE using definite length form.
 */
public final class DLSequence extends ASN1Sequence {
    private int contentsLength = -1;

    /**
     * Create an empty sequence
     */
    public DLSequence() {
    }

    /**
     * create a sequence containing a vector of objects.
     *
     * @param elementVector the vector of objects to make up the sequence.
     */
    public DLSequence(ASN1EncodableVector elementVector) {
        super(elementVector);
    }

    /**
     * create a sequence containing an array of objects.
     *
     * @param elements the array of objects to make up the sequence.
     */
    public DLSequence(ASN1Encodable[] elements) {
        super(elements);
    }


    private int getContentsLength() throws IOException {
        if (contentsLength < 0) {
            int totalLength = 0;

            for (ASN1Encodable element : elements) {
                ASN1Primitive dlObject = element.toASN1Primitive().toDLObject();
                totalLength += dlObject.encodedLength(true);
            }

            this.contentsLength = totalLength;
        }

        return contentsLength;
    }

    int encodedLength(boolean withTag) throws IOException {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, getContentsLength());
    }

    /**
     * A note on the implementation:
     * <p>
     * As DL requires the constructed, definite-length model to
     * be used for structured types, this varies slightly from the
     * ASN.1 descriptions given. Rather than just outputting SEQUENCE,
     * we also have to specify CONSTRUCTED, and the objects length.
     */
    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeIdentifier(withTag, BERTags.CONSTRUCTED | BERTags.SEQUENCE);

        ASN1OutputStream dlOut = out.getDLSubStream();

        int count = elements.length;
        if (contentsLength >= 0 || count > 16) {
            out.writeDL(getContentsLength());

            for (ASN1Encodable element : elements) {
                dlOut.writePrimitive(element.toASN1Primitive());
            }
        } else {
            int totalLength = 0;

            ASN1Primitive[] dlObjects = new ASN1Primitive[count];
            for (int i = 0; i < count; ++i) {
                ASN1Primitive dlObject = elements[i].toASN1Primitive().toDLObject();
                dlObjects[i] = dlObject;
                totalLength += dlObject.encodedLength(true);
            }

            this.contentsLength = totalLength;
            out.writeDL(totalLength);

            for (int i = 0; i < count; ++i) {
                dlOut.writePrimitive(dlObjects[i]);
            }
        }
    }

    ASN1Primitive toDLObject() {
        return this;
    }
}
