package threads.lite.cert;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Objects;
import java.util.Set;
import java.util.Vector;

/**
 * Generator for X.509 extensions
 */
public final class ExtensionsGenerator {
    private static final Set<ASN1ObjectIdentifier> dupsAllowed;

    static {
        dupsAllowed = Set.of(Extension.subjectAlternativeName, Extension.issuerAlternativeName,
                Extension.subjectDirectoryAttributes, Extension.certificateIssuer);
    }

    private final Hashtable<ASN1ObjectIdentifier, Extension> extensions = new Hashtable<>();
    private final Vector<ASN1ObjectIdentifier> extOrdering = new Vector<>();

    /**
     * Add an extension with the given oid and the passed in value to be included
     * in the OCTET STRING associated with the extension.
     *
     * @param oid      OID for the extension.
     * @param critical true if critical, false otherwise.
     * @param value    the ASN.1 object to be included in the extension.
     */
    public void addExtension(
            ASN1ObjectIdentifier oid,
            boolean critical,
            ASN1Encodable value)
            throws IOException {
        this.addExtension(oid, critical, value.toASN1Primitive().getEncoded(ASN1Encoding.DER));
    }

    /**
     * Add an extension with the given oid and the passed in byte array to be wrapped in the
     * OCTET STRING associated with the extension.
     *
     * @param oid      OID for the extension.
     * @param critical true if critical, false otherwise.
     * @param value    the byte array to be wrapped.
     */
    private void addExtension(
            ASN1ObjectIdentifier oid,
            boolean critical,
            byte[] value) {
        if (extensions.containsKey(oid)) {
            if (dupsAllowed.contains(oid)) {
                Extension existingExtension = extensions.get(oid);
                ASN1Sequence seq1 = ASN1Sequence.getInstance(DEROctetString.getInstance(
                        Objects.requireNonNull(existingExtension).getExtnValue()).getOctets());
                ASN1Sequence seq2 = ASN1Sequence.getInstance(value);

                ASN1EncodableVector items = new ASN1EncodableVector(seq1.size() + seq2.size());

                for (ASN1Encodable element : seq1.toArrayInternal()) {
                    items.add(element);
                }
                for (ASN1Encodable element : seq2.toArrayInternal()) {
                    items.add(element);
                }

                try {
                    extensions.put(oid, new Extension(oid, critical, new DERSequence(items).getEncoded()));
                } catch (IOException e) {
                    throw new ASN1ParsingException(e.getMessage(), e);
                }
            } else {
                throw new IllegalArgumentException("extension " + oid + " already added");
            }
        } else {
            extOrdering.addElement(oid);
            extensions.put(oid, new Extension(oid, critical, new DEROctetString(Arrays.clone(value))));
        }
    }

    /**
     * Return true if there are no extension present in this generator.
     *
     * @return true if empty, false otherwise
     */
    public boolean isEmpty() {
        return extOrdering.isEmpty();
    }

    /**
     * Generate an Extensions object based on the current state of the generator.
     *
     * @return an X09Extensions object.
     */
    public Extensions generate() {
        Extension[] exts = new Extension[extOrdering.size()];

        for (int i = 0; i != extOrdering.size(); i++) {
            exts[i] = extensions.get(extOrdering.elementAt(i));
        }

        return new Extensions(exts);
    }

}
