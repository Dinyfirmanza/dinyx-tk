package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public record PeerInfo(@NonNull PeerId peerId,
                       @NonNull String agent,
                       @NonNull Multiaddr[] multiaddrs,
                       @NonNull String[] protocols,
                       @Nullable Multiaddr observed) {

    @NonNull
    @Override
    public String toString() {
        return "PeerInfo{" +
                " peerId='" + peerId + '\'' +
                ", agent='" + agent + '\'' +
                ", addresses=" + Arrays.toString(multiaddrs) +
                ", protocols=" + Arrays.toString(protocols) +
                ", observed=" + observed +
                '}';
    }

    public boolean hasProtocol(String protocol) {
        return Arrays.asList(protocols).contains(protocol);
    }

    public boolean hasRelayHop() {
        return hasProtocol(IPFS.RELAY_PROTOCOL_HOP);
    }

}
