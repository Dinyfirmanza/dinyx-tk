package threads.lite.core;

import androidx.annotation.NonNull;


public record Keys(@NonNull byte[] publicKey, @NonNull byte[] privateKey) {
}
