package threads.lite.core;

public record Limit(long data, long duration, boolean limited) {
}
