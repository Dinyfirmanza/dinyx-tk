package threads.lite.cid;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import java.io.ByteArrayOutputStream;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import threads.lite.core.DataHandler;

@Entity
public record Multiaddr(
        @PrimaryKey @NonNull @ColumnInfo(name = "peerId") @TypeConverters(PeerId.class) PeerId peerId,
        @NonNull @ColumnInfo(name = "address", typeAffinity = ColumnInfo.BLOB) byte[] address)
        implements Comparable<Multiaddr> {

    private static final Set<Protocol> ANY_DNS = Set.of(Protocol.DNSADDR, Protocol.DNS,
            Protocol.DNS4, Protocol.DNS6);

    public Multiaddr(@NonNull PeerId peerId, byte[] address) {
        this.peerId = peerId;
        this.address = address;
    }


    @NonNull
    public static Multiaddr getLoopbackAddress(PeerId peerId, int port) {
        return Multiaddr.create(peerId, new InetSocketAddress(
                InetAddress.getLoopbackAddress(), port));
    }

    @NonNull
    public static List<Multiaddr> getSiteLocalAddresses(PeerId peerId, int port) throws Exception {
        List<InetSocketAddress> inetSocketAddresses = Network.getSiteLocalAddress(port);
        List<Multiaddr> result = new ArrayList<>();
        for (InetSocketAddress inetSocketAddress : inetSocketAddresses) {
            result.add(create(peerId, inetSocketAddress));
        }
        return result;
    }

    @NonNull
    public static Multiaddr create(String address) throws Exception {
        String[] strings = address.split("/");
        if (strings.length == 0) {
            throw new Exception("Address has not a separator");
        }
        String empty = strings[0];
        if (!empty.isEmpty()) {
            throw new Exception("Address should start with separator '/'");
        }
        int size = strings.length;
        int last = size - 1;
        int udp = last - 1;

        PeerId peerId = PeerId.decode(strings[last]); // last element always a peerId
        Objects.requireNonNull(peerId);


        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // skip udp and peerId
        for (int i = 1; i < udp; i++) {
            String part = strings[i];
            Protocol protocol = Protocol.lookup(part);
            Protocol.encodeProtocol(protocol, byteArrayOutputStream);
            if (protocol.size() == 0) continue;

            i++;
            String component = strings[i];
            if (component == null || component.isEmpty())
                throw new Exception("Protocol requires address, but non provided!");

            protocol.partToStream(component, byteArrayOutputStream);
        }
        return new Multiaddr(peerId, byteArrayOutputStream.toByteArray());
    }

    @NonNull
    public static Multiaddr createCircuit(PeerId peerId, Multiaddr relayAddr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(relayAddr.address());
            Protocol.encodeProtocol(Protocol.P2P, byteArrayOutputStream);
            Protocol.encodePart(relayAddr.peerId(), byteArrayOutputStream);
            Protocol.encodeProtocol(Protocol.P2PCIRCUIT, byteArrayOutputStream);
            return new Multiaddr(peerId, byteArrayOutputStream.toByteArray());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Nullable
    public static Multiaddr create(PeerId peerId, byte[] raw) {
        byte[] parseAddress = Protocol.parseAddress(CodedInputStream.newInstance(raw), peerId);
        if (parseAddress.length == 0) return null;
        return new Multiaddr(peerId, parseAddress);
    }

    // circuit addresses are sorted out here
    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings, IPV ipv) {
        Multiaddrs result = new Multiaddrs();
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings);
        for (Multiaddr addr : multiaddrs) {
            if (!addr.isCircuitAddress()) {
                if (addr.protocolSupported(ipv)) {
                    result.add(addr);
                }
            }
        }
        return result;
    }

    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = new Multiaddrs();
        for (ByteString entry : byteStrings) {
            Multiaddr multiaddr = Multiaddr.create(peerId, entry.toByteArray());
            if (multiaddr != null) {
                multiaddrs.add(multiaddr);
            }
        }
        return multiaddrs;
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        return create(peerId, inetSocketAddress.getAddress(), inetSocketAddress.getPort());
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, InetAddress inetAddress, int port) {
        boolean ipv6 = inetAddress instanceof Inet6Address;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (ipv6) {
                Protocol.encodeProtocol(Protocol.IP6, byteArrayOutputStream);
            } else {
                Protocol.encodeProtocol(Protocol.IP4, byteArrayOutputStream);
            }
            Protocol.encodePart(inetAddress, byteArrayOutputStream);
            Protocol.encodeProtocol(Protocol.UDP, byteArrayOutputStream);
            Protocol.encodePart(port, byteArrayOutputStream);
            Protocol.encodeProtocol(Protocol.QUICV1, byteArrayOutputStream);

            return new Multiaddr(peerId, byteArrayOutputStream.toByteArray());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    @NonNull
    public InetSocketAddress inetSocketAddress() throws UnknownHostException {
        return new InetSocketAddress(inetAddress(), port());
    }

    private boolean hasQuic() {
        return has(Set.of(Protocol.QUICV1));
    }

    public boolean protocolSupported(IPV ipv) {

        if (ipv == IPV.IPv4) {
            if (isIP6() || isDns6()) {
                return false;
            }
        }
        if (ipv == IPV.IPv6) {
            if (isIP4() || isDns4()) {
                return false;
            }
        }

        if (isDnsaddr()) {
            return true;
        }
        if (isDns()) {
            return hasQuic();
        }
        if (isDns4()) {
            return hasQuic();
        }
        if (isDns6()) {
            return hasQuic();
        }
        if (hasQuic()) {
            Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                    Set.of(Protocol.IP4, Protocol.IP6));
            InetAddress inetAddress = ((InetAddress) Objects.requireNonNull(part));

            if (Network.isWellKnownIPv4Translatable(inetAddress)) {
                return false;
            }
            return !Network.isLocalAddress(inetAddress);
        }
        return false;
    }


    @NonNull
    public byte[] address() {
        return address;
    }


    // only invoke on dns* addresses
    @NonNull
    public String dnsHost() {
        Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                Set.of(Protocol.DNS, Protocol.DNS4, Protocol.DNS6, Protocol.DNSADDR));
        if (part instanceof String host) {
            return host;
        } else {
            throw new IllegalStateException("unknown host (only dns is supported)");
        }
    }

    @NonNull
    public String host() {
        if (isAnyDns()) {
            Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                    Set.of(Protocol.DNS, Protocol.DNS4, Protocol.DNS6, Protocol.DNSADDR));
            return (String) Objects.requireNonNull(part);
        } else {
            Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                    Set.of(Protocol.IP4, Protocol.IP6));
            return ((InetAddress) Objects.requireNonNull(part)).getHostName();
        }
    }

    // only invoke on ip4 or ip6 addresses
    @NonNull
    public InetAddress inetAddress() throws UnknownHostException {
        if (isAnyDns()) {
            Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                    Set.of(Protocol.DNS, Protocol.DNS4, Protocol.DNS6, Protocol.DNSADDR));
            return InetAddress.getByName((String) Objects.requireNonNull(part));
        } else {
            Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                    Set.of(Protocol.IP4, Protocol.IP6));
            return ((InetAddress) Objects.requireNonNull(part));
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Multiaddr multiaddr = (Multiaddr) o;
        return Objects.equals(peerId, multiaddr.peerId) && Arrays.equals(address, multiaddr.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(address), peerId); // ok, checked, maybe opt
    }

    @NonNull
    @Override
    public String toString() {
        return Protocol.getString(CodedInputStream.newInstance(address)) +
                "/" + Protocol.P2P.type() + "/" + peerId;
    }

    private boolean has(Set<Protocol> types) {
        return Protocol.has(CodedInputStream.newInstance(address), types);
    }


    public boolean isIP4() {
        return transport() == Protocol.IP4;
    }

    private boolean isIP6() {
        return transport() == Protocol.IP6;
    }

    public boolean isDns() {
        return transport() == Protocol.DNS;
    }

    public boolean isDns6() {
        return transport() == Protocol.DNS6;
    }

    public boolean isDns4() {
        return transport() == Protocol.DNS4;
    }

    public boolean isDnsaddr() {
        return transport() == Protocol.DNSADDR;
    }

    public boolean isAnyDns() {
        return ANY_DNS.contains(transport());
    }

    public boolean isCircuitAddress() {
        return has(Set.of(Protocol.P2PCIRCUIT));
    }

    @Override
    @NonNull
    public PeerId peerId() {
        return peerId;
    }

    @NonNull
    public PeerId getRelayId() {
        if (!isCircuitAddress()) {
            throw new IllegalStateException("no circuit address");
        }
        Object part = Protocol.getValue(CodedInputStream.newInstance(address),
                Set.of(Protocol.P2P)); // its the first one
        if (part instanceof PeerId) {
            return (PeerId) part;
        }
        throw new IllegalStateException("no valid circuit address");
    }

    @NonNull
    public Multiaddr getRelayAddress() {
        if (!isCircuitAddress()) {
            throw new IllegalStateException("not a circuit address");
        }
        PeerId relayId = getRelayId();
        Objects.requireNonNull(relayId);
        byte[] parseAddress = Protocol.parseAddress(
                CodedInputStream.newInstance(address), relayId);
        return new Multiaddr(relayId, parseAddress);
    }

    // this function returns the first part (its always Protocol.IP4, Protocol.IP6,
    // Protocol.DNS, Protocol.DNS4, Protocol.DNS6, Protocol.DNSADDR etc)
    @NonNull
    Protocol transport() {
        int transport = address[0]; // always fit in the first byte
        Protocol protocol = Protocol.lookup(transport);
        Objects.requireNonNull(protocol);
        return protocol;
    }

    //  on a dnsaddr addresses it will return -1
    public int port() {
        if (isDnsaddr()) {
            return -1;
        }
        Object part = Protocol.getValue(CodedInputStream.newInstance(address), Set.of(Protocol.UDP));
        Objects.requireNonNull(part);
        return (Integer) part;
    }

    @Override
    public int compareTo(Multiaddr o) {
        return Comparator.comparing(Multiaddr::transport)
                .thenComparing(Multiaddr::peerId)
                .thenComparing((o1, o2) -> DataHandler.compareUnsigned(o1.address, o2.address))
                .compare(this, o);
    }
}
