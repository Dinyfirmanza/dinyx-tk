package threads.lite.cid;

public record Hashtype(int index, int length) {

    public static final Hashtype ID = new Hashtype(0, -1);
    public static final Hashtype SHA2_256 = new Hashtype(0x12, 32);

    public static Hashtype lookup(int type) {
        switch (type) {
            case 0 -> {
                return ID;
            }
            case 0x12 -> {
                return SHA2_256;
            }
            default -> throw new IllegalArgumentException("unsupported hashtype " + type);
        }
    }
}
