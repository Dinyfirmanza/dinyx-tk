package threads.lite.cid;

public record Dir(Cid cid, long size) {
}
