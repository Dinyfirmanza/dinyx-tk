package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.google.protobuf.ByteString;

import java.util.Objects;

import multiaddr.pb.MultiaddrOuterClass;

@Entity
public record Peer(@NonNull @PrimaryKey @ColumnInfo(name = "id") @TypeConverters(ID.class) ID id,
                   @NonNull @ColumnInfo(name = "multiaddr") @TypeConverters(Peer.class) Multiaddr multiaddr,
                   @ColumnInfo(name = "replaceable") boolean replaceable) {

    // Note: fore creating a peer instance use the static create method
    public Peer(@NonNull ID id, @NonNull Multiaddr multiaddr, boolean replaceable) {
        this.id = id;
        this.multiaddr = multiaddr;
        this.replaceable = replaceable;
    }

    @NonNull
    @TypeConverter
    public static Multiaddr fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }
        try {
            MultiaddrOuterClass.Multiaddr stored =
                    MultiaddrOuterClass.Multiaddr.parseFrom(data);
            PeerId peerId = new PeerId(stored.getId().toByteArray());
            Multiaddr multiaddr = new Multiaddr(peerId, stored.getAddrs().toByteArray());
            Objects.requireNonNull(multiaddr);
            return multiaddr;
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Multiaddr multiaddr) {
        if (multiaddr == null) {
            throw new IllegalStateException("multiaddr can not be null");
        }
        return MultiaddrOuterClass.Multiaddr.newBuilder()
                .setId(ByteString.copyFrom(multiaddr.peerId().encoded()))
                .setAddrs(ByteString.copyFrom(multiaddr.address()))
                .build().toByteArray();

    }

    @NonNull
    public static Peer create(@NonNull Multiaddr multiaddr, boolean replaceable) throws Exception {
        ID id = ID.convertPeerID(multiaddr.peerId());
        return new Peer(id, multiaddr, replaceable);
    }

    @Override
    @NonNull
    public ID id() {
        return id;
    }

    @NonNull
    public PeerId getPeerId() {
        return multiaddr.peerId();
    }

    @Override
    @NonNull
    public Multiaddr multiaddr() {
        return multiaddr;
    }

    @NonNull
    @Override
    public String toString() {
        return "Peer{" +
                "multiaddr=" + multiaddr +
                ", id=" + id +
                ", replaceable=" + replaceable +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return id.equals(peer.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode(); // ok, checked, maybe opt
    }

    @Override
    public boolean replaceable() {
        return replaceable;
    }


}
