package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import record.pb.EnvelopeOuterClass;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.cid.PeerId;
import threads.lite.cid.Record;
import threads.lite.core.NatType;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;


@RunWith(AndroidJUnit4.class)
public class IpfsRelayTest {
    private static final String TAG = IpfsRelayTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_holepunch_connect() throws Throwable {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (server.getNatType() != NatType.RESTRICTED_CONE) {
            LogUtils.warning(TAG, "nothing to test here NAT TYPE is not RESTRICTED_CONE");
            return;
        }

        assertTrue(IPFS.hasReservations(server));
        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            Set<Reservation> reservations = IPFS.reservations(server);
            assertFalse(reservations.isEmpty());
            Reservation reservation = reservations.stream().
                    findAny().orElseThrow((Supplier<Throwable>) () ->
                            new RuntimeException("at least one present"));


            PeerId relayID = reservation.getRelayId();
            assertNotNull(relayID);
            Multiaddr relay = reservation.relayAddress();
            assertNotNull(relay);

            assertTrue(reservation.getLimitData() > 0);
            assertTrue(reservation.getLimitDuration() > 0);
            Multiaddr circuitMultiaddr = reservation.circuitAddress();
            assertNotNull(circuitMultiaddr);

            Connection dummyConnection = dummySession.dial(circuitMultiaddr, Parameters.getDefault());
            Objects.requireNonNull(dummyConnection);

            // TEST 1
            PeerInfo peerInfo = dummy.getHost().getPeerInfo(dummyConnection);
            assertNotNull(peerInfo);
            assertEquals(peerInfo.agent(), IPFS.AGENT);
            LogUtils.debug(TAG, peerInfo.toString());


            // TEST 2
            AtomicBoolean success = new AtomicBoolean(false);
            byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
            EnvelopeOuterClass.Envelope data = IPFS.createEnvelope(dummySession, Record.LITE, test);

            ipfs.setIncomingPush(push -> {
                EnvelopeOuterClass.Envelope envelope = push.envelope();
                assertNotNull(push.connection());
                try {
                    IPFS.verifyEnvelope(Record.LITE, envelope);
                    success.set(Arrays.equals(envelope.getPayload().toByteArray(), test));
                } catch (Exception e) {
                    success.set(false);
                }

            });

            IPFS.push(dummyConnection, data);
            Thread.sleep(2000);
            Assert.assertTrue(success.get());

            // TEST 3
            peerInfo = ipfs.getPeerInfo(dummyConnection);
            assertNotNull(peerInfo);

            // TEST 4
            success.set(false);
            byte[] test2 = "zehn".getBytes(StandardCharsets.UTF_8);
            EnvelopeOuterClass.Envelope data2 = IPFS.createEnvelope(dummySession, Record.LITE, test2);

            ipfs.setIncomingPush(push -> {
                EnvelopeOuterClass.Envelope envelope = push.envelope();
                success.set(Arrays.equals(envelope.getPayload().toByteArray(), test2));
            });


            IPFS.push(dummyConnection, data2);
            Thread.sleep(2000);
            Assert.assertTrue(success.get());

            // will close the relay connection from the dummy
            dummyConnection.close();

            assertTrue(IPFS.hasReservations(server));

        } finally {
            dummy.clearDatabase();
        }
    }

}
