package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;

@RunWith(AndroidJUnit4.class)
public class IpfsClientServerTest {


    private static final String TAG = IpfsClientServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void server_stress_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);

        try (Session session = ipfs.createSession()) {
            PeerId host = ipfs.self();
            assertNotNull(host);
            Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());

            byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

            Cid cid = IPFS.storeData(session, input);
            assertNotNull(cid);


            try (Session dummySession = dummy.createSession()) {

                Connection connection = dummySession.dial(multiaddr, Parameters.getDefault());
                Objects.requireNonNull(connection);

                Thread.sleep(1000);

                assertEquals(server.numServerConnections(), 1);

                PeerInfo info = dummy.getHost().getPeerInfo(connection);
                assertNotNull(info);
                assertEquals(info.agent(), IPFS.AGENT);

                //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
                byte[] output = IPFS.getData(dummySession, cid, new Progress() {
                    @Override
                    public void setProgress(int progress) {
                        LogUtils.verbose(TAG, String.valueOf(progress));
                    }

                    @Override
                    public boolean isCancelled() {
                        return false;
                    }
                });
                assertArrayEquals(input, output);

                connection.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numServerConnections(), 0);
        }

    }


    @Test
    public void big_server_stress_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        try (Session session = ipfs.createSession()) {
            Dummy dummy = Dummy.getInstance(context);

            PeerId host = ipfs.self();
            assertNotNull(host);
            Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());

            int packetSize = 1000;
            long maxData = 100000;
            File inputFile = TestEnv.createCacheFile(context);
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            Cid cid;
            try (InputStream inputStream = new FileInputStream(inputFile)) {
                cid = IPFS.storeInputStream(session, inputStream);
            }
            assertNotNull(cid);
            try (Session dummySession = dummy.createSession()) {

                Connection connection = dummySession.dial(multiaddr, Parameters.getDefault());
                Objects.requireNonNull(connection);

                Thread.sleep(1000);

                assertEquals(server.numServerConnections(), 1);


                File temp = TestEnv.createCacheFile(context);
                IPFS.fetchToFile(dummySession, temp, cid, () -> false);

                TestCase.assertEquals(temp.length(), inputFile.length());

                assertTrue(temp.delete());

                connection.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numServerConnections(), 0);
        }

    }
}
