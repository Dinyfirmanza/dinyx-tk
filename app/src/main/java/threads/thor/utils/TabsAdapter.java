package threads.thor.utils;


import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.core.tabs.Tab;

public class TabsAdapter extends RecyclerView.Adapter<TabsAdapter.ImageViewHolder> {

    private final ViewHolderListener viewHolderListener;
    private final List<Tab> tabs = new ArrayList<>();

    public TabsAdapter(ViewHolderListener viewHolderListener) {
        this.viewHolderListener = viewHolderListener;
    }

    public void updateData(@NonNull List<Tab> tabs) {
        final TabDiffCallback diffCallback = new TabDiffCallback(this.tabs, tabs);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.tabs.clear();
        this.tabs.addAll(tabs);
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_tab, parent, false);
        return new ImageViewHolder(view, viewHolderListener);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.onBind(position);
    }

    public long getItemId(int position) {
        if (position < 0 || position >= getItemCount()) {
            return 0;
        }
        return tabs.get(position).idx();
    }

    @Override
    public int getItemCount() {
        return tabs.size();
    }


    public interface ViewHolderListener {

        void onItemClicked(int position);

        void onItemDismiss(Tab tab);

        boolean isSelected(int position);
    }


    class ImageViewHolder extends RecyclerView.ViewHolder {

        private final ShapeableImageView image;
        private final TextView title;
        private final TextView uri;
        private final MaterialCardView cardView;
        private final ViewHolderListener listener;

        ImageViewHolder(View itemView, ViewHolderListener listener) {
            super(itemView);
            this.listener = listener;
            this.cardView = itemView.findViewById(R.id.card_view);
            this.image = itemView.findViewById(R.id.card_image);
            this.title = itemView.findViewById(R.id.card_title);
            this.uri = itemView.findViewById(R.id.card_uri);
        }

        void onBind(int position) {

            Tab tab = tabs.get(position);

            this.title.setText(tab.title());
            this.uri.setText(tab.uri());

            Bitmap image = tab.bitmap();
            if (image != null) {
                this.image.setImageBitmap(image);
            } else {
                this.image.setImageResource(DOCS.getImageResource(Uri.parse(tab.uri())));
            }
            this.cardView.setFocusable(listener.isSelected(position));
            this.cardView.setSelected(listener.isSelected(position));
            this.cardView.setChecked(listener.isSelected(position));
            this.cardView.setOnClickListener(v -> listener.onItemClicked(position));

            SwipeDismissBehavior<View> swipeDismissBehavior = new SwipeDismissBehavior<>();
            swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_END_TO_START);


            CoordinatorLayout.LayoutParams coordinatorParams =
                    (CoordinatorLayout.LayoutParams) cardView.getLayoutParams();

            coordinatorParams.setBehavior(swipeDismissBehavior);

            swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
                @Override
                public void onDismiss(View view) {
                    listener.onItemDismiss(tab);
                }

                @Override
                public void onDragStateChanged(int state) {
                    switch (state) {
                        case SwipeDismissBehavior.STATE_DRAGGING, SwipeDismissBehavior.STATE_SETTLING ->
                                cardView.setDragged(true);
                        case SwipeDismissBehavior.STATE_IDLE -> cardView.setDragged(false);
                        default -> {
                        }
                    }
                }
            });
        }
    }

}