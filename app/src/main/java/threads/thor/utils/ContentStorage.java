package threads.thor.utils;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ThreadLocalRandom;

import threads.lite.LogUtils;
import threads.magnet.data.Storage;
import threads.magnet.data.StorageUnit;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.TorrentFile;
import threads.thor.core.DOCS;

public class ContentStorage implements Storage {

    private static final String TAG = ContentStorage.class.getSimpleName();
    private final EventBus eventBus;
    private final DOCS docs;
    private final String root;

    public ContentStorage(@NonNull Context context, @NonNull EventBus eventbus, @NonNull String root) throws Exception {
        this.eventBus = eventbus;
        this.docs = DOCS.getInstance(context);
        this.root = root;
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    @NonNull
    public File getRootFile() {
        File dataDir = docs.getDataDir();
        if (!dataDir.exists()) {
            throw new IllegalArgumentException("data directory does not exists");
        }
        File rootDir = new File(dataDir, root);
        if (!rootDir.exists()) {
            boolean created = rootDir.mkdir();
            if (!created) {
                throw new IllegalArgumentException("root directory couldn't be created");
            }
        }
        return rootDir;
    }

    @NonNull
    EventBus getEventBus() {
        return eventBus;
    }

    @Override
    public StorageUnit getUnit(@NonNull TorrentFile torrentFile) {
        try {
            return new ContentStorageUnit(this, torrentFile);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            throw new IllegalArgumentException(e);
        }
    }
}
