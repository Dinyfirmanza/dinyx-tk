package threads.thor.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import threads.thor.core.tabs.Tab;


public final class TabDiffCallback extends DiffUtil.Callback {
    private final List<Tab> mOldList;
    private final List<Tab> mNewList;

    public TabDiffCallback(List<Tab> tabs, List<Tab> newTabs) {
        this.mOldList = tabs;
        this.mNewList = newTabs;
    }

    private static boolean sameContent(@NonNull Tab t, @NonNull Tab o) {
        return t.idx() == o.idx();
    }

    private static boolean areItemsTheSame(@NonNull Tab t, @NonNull Tab o) {
        return t.idx() == o.idx();
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return sameContent(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

}
