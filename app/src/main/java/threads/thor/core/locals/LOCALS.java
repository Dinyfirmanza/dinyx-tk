package threads.thor.core.locals;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.mdns.MDNS;

public class LOCALS {
    private static volatile LOCALS INSTANCE = null;
    @NonNull
    private final ConcurrentHashMap<PeerId, MDNS.Peer> locals = new ConcurrentHashMap<>();

    public static LOCALS getInstance() {

        if (INSTANCE == null) {
            synchronized (LOCALS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new LOCALS();
                }
            }
        }
        return INSTANCE;
    }

    public void addPeer(@NonNull MDNS.Peer peer) {
        locals.put(peer.multiaddr().peerId(), peer);
    }

    @Nullable
    public MDNS.Peer getPeer(@NonNull PeerId peerId) {
        return locals.get(peerId);
    }


    @NonNull
    public Multiaddrs locals() {
        Multiaddrs multiaddrs = new Multiaddrs();
        for (MDNS.Peer peer : locals.values()) {
            multiaddrs.add(peer.multiaddr());
        }
        return multiaddrs;
    }
}
