package threads.thor.core.events;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

public class EVENTS {
    public static final String FATAL = "FATAL";
    public static final String ERROR = "ERROR";
    public static final String WARNING = "WARNING";
    public static final String INFO = "INFO";
    public static final String PERMISSION = "PERMISSION";
    private static volatile EVENTS INSTANCE = null;
    private final EventsDatabase eventsDatabase;

    private EVENTS(EventsDatabase eventsDatabase) {
        this.eventsDatabase = eventsDatabase;
    }


    public static EVENTS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (EVENTS.class) {
                if (INSTANCE == null) {
                    EventsDatabase eventsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            EventsDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = new EVENTS(eventsDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    private static Event createEvent(@NonNull String identifier, @NonNull String content) {

        return Event.createEvent(identifier, content);
    }

    private void storeEvent(@NonNull Event event) {
        eventsDatabase.eventDao().insertEvent(event);
    }


    public void fatal(@NonNull String content) {
        storeEvent(createEvent(FATAL, content));
    }

    public void error(@NonNull String content) {
        storeEvent(createEvent(ERROR, content));
    }

    public void permission(@NonNull String content) {
        storeEvent(createEvent(PERMISSION, content));
    }

    public void info(@NonNull String content) {
        storeEvent(createEvent(INFO, content));
    }

    public void warning(@NonNull String content) {
        storeEvent(createEvent(WARNING, content));
    }

    public EventsDatabase getEventsDatabase() {
        return eventsDatabase;
    }

}
