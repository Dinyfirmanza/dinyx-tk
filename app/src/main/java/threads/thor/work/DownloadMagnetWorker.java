package threads.thor.work;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.core.Progress;
import threads.magnet.Client;
import threads.magnet.ClientBuilder;
import threads.magnet.IdentityService;
import threads.magnet.Runtime;
import threads.magnet.event.EventBus;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.magnet.net.PeerId;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.core.DOCS;
import threads.thor.utils.ContentStorage;
import threads.thor.utils.MimeTypeService;

public final class DownloadMagnetWorker extends Worker {

    private static final String TAG = DownloadMagnetWorker.class.getSimpleName();

    /**
     * @noinspection WeakerAccess
     */
    public DownloadMagnetWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri magnet) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(DOCS.MAGNET_SCHEME, magnet.toString());

        return new OneTimeWorkRequest.Builder(DownloadMagnetWorker.class)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .build();

    }

    public static void download(@NonNull Context context, @NonNull Uri magnet) {
        WorkManager.getInstance(context).enqueue(getWork(magnet));
    }

    @Nullable
    private static Uri downloadsUri(Context context, String mimeType, String name, String path) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, name);
        contentValues.put(MediaStore.Images.ImageColumns.MIME_TYPE, mimeType);
        contentValues.put(MediaStore.Images.ImageColumns.RELATIVE_PATH, path);
        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");

        try {
            String magnet = getInputData().getString(DOCS.MAGNET_SCHEME);
            Objects.requireNonNull(magnet);

            MagnetUri magnetUri = MagnetUriParser.parse(magnet);

            String name = magnet;
            if (magnetUri.getDisplayName().isPresent()) {
                name = magnetUri.getDisplayName().get();
            }
            String display = name;

            setForegroundAsync(createForegroundInfo(display, 0));


            byte[] id = new IdentityService().getID();

            EventBus eventBus = Runtime.provideEventBus();
            ContentStorage storage = new ContentStorage(
                    getApplicationContext(), eventBus, name);
            Runtime runtime = new Runtime(PeerId.fromBytes(id), eventBus,
                    ContentStorage.nextFreePort());

            Client client = new ClientBuilder().runtime(runtime)
                    .storage(storage).magnet(magnet).build();

            AtomicInteger atomicProgress = new AtomicInteger(0);
            client.startAsync((torrentSessionState) -> {

                long completePieces = torrentSessionState.getPiecesComplete();
                long totalPieces = torrentSessionState.getPiecesTotal();
                int progress = (int) ((completePieces * 100.0f) / totalPieces);

                LogUtils.info(TAG, "progress : " + progress +
                        " pieces : " + completePieces + "/" + totalPieces);

                if (atomicProgress.getAndSet(progress) < progress) {
                    setForegroundAsync(createForegroundInfo(display, progress));
                }
                if (isStopped()) {
                    try {
                        client.stop();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        LogUtils.info(TAG, "Client is stopped !!!");
                    }
                }
            }, 1000).join();

            if (!isStopped()) {
                File file = storage.getRootFile();
                Stack<String> dirs = new Stack<>();
                if (file.isDirectory()) {
                    uploadDirectory(file, dirs);
                } else {
                    uploadFile(file, dirs);
                }
                DOCS.deleteFile(file, true);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }

    private void uploadFile(@NonNull File file, @NonNull Stack<String> dirs) throws IOException {
        String name = file.getName();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator + String.join((File.separator), dirs);

        Uri downloads = downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        try (InputStream is = new FileInputStream(file)) {
            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                Objects.requireNonNull(os, "Failed to open output stream.");

                IPFS.copy(is, os, new Progress() {
                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                    @Override
                    public void setProgress(int progress) {
                        setForegroundAsync(createForegroundInfo(name, progress));
                    }
                }, file.length());
            }
        } catch (Throwable throwable) {
            contentResolver.delete(downloads, null, null);
            throw throwable;
        }

    }

    private void uploadDirectory(@NonNull File dir, Stack<String> dirs) throws IOException {
        dirs.add(dir.getName());
        String[] children = dir.list();
        if (children != null) {
            for (String child : children) {
                File fileChild = new File(dir, child);
                if (fileChild.isDirectory()) {
                    uploadDirectory(fileChild, dirs);
                } else {
                    uploadFile(fileChild, dirs);
                }
            }
        }
        dirs.pop();
    }

    @NonNull
    private ForegroundInfo createForegroundInfo(@NonNull String name, int progress) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);

        PendingIntent cancelPendingIntent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent intent = InitApplication.getDownloadsIntent();

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                cancelPendingIntent).build();

        builder.setContentTitle(name)
                .setSubText(progress + "%")
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setOngoing(true)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true);


        Notification notification = builder.build();

        int notificationId = getId().hashCode();
        return new ForegroundInfo(notificationId, notification);
    }

}
