package threads.magnet.peerexchange;

import java.time.Duration;

interface PeerExchangeConfig {

    Duration minMessageInterval = Duration.ofMinutes(1);
    Duration maxMessageInterval = Duration.ofMinutes(5);


    /**
     * @noinspection SameReturnValue
     */
    static Duration getMinMessageInterval() {
        return minMessageInterval;
    }


    /**
     * @noinspection SameReturnValue
     */
    static Duration getMaxMessageInterval() {
        return maxMessageInterval;
    }

}
