package threads.magnet.peerexchange;

import androidx.annotation.NonNull;

import threads.magnet.net.Peer;

record PeerEvent(Type type, Peer peer, long instant) implements Comparable<PeerEvent> {


    private static PeerEvent createPeerEvent(Type type, Peer peer) {
        return new PeerEvent(type, peer, System.currentTimeMillis());
    }

    static PeerEvent added(Peer peer) {
        return PeerEvent.createPeerEvent(Type.ADDED, peer);
    }

    static PeerEvent dropped(Peer peer) {
        return PeerEvent.createPeerEvent(Type.DROPPED, peer);
    }

    long getInstant() {
        return instant;
    }

    @Override
    public int compareTo(PeerEvent o) {

        if (instant == o.instant) {
            return 0;
        } else if (instant - o.instant >= 0) {
            return 1;
        } else {
            return -1;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "PeerEvent {type=" + type + ", peer=" + peer + ", instant=" + instant + '}';
    }

    enum Type {ADDED, DROPPED}
}
