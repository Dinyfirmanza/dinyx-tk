package threads.magnet.torrent;

import java.util.function.IntPredicate;

import threads.magnet.data.Bitfield;

public record IncompletePiecesValidator(Bitfield bitfield) implements IntPredicate {

    @Override
    public boolean test(int pieceIndex) {
        return !isComplete(pieceIndex);
    }

    private boolean isComplete(int pieceIndex) {
        Bitfield.PieceStatus pieceStatus = bitfield.getPieceStatus(pieceIndex);
        return pieceStatus == Bitfield.PieceStatus.COMPLETE || pieceStatus == Bitfield.PieceStatus.COMPLETE_VERIFIED;
    }
}
