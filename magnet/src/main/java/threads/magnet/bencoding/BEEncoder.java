package threads.magnet.bencoding;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * BEncoding encoder.
 *
 * @since 1.0
 */
interface BEEncoder {


    /**
     * Write bencoded string to a binary output.
     *
     * @since 1.0
     */
    static void encode(BEString string, OutputStream out) throws IOException {

        Objects.requireNonNull(string);

        byte[] bytes = string.getValue();
        encodeString(bytes, out);
    }

    private static void encodeString(byte[] bytes, OutputStream out) throws IOException {
        write(out, Integer.toString(bytes.length).getBytes(StandardCharsets.UTF_8));
        write(out, ':');
        write(out, bytes);
    }

    /**
     * Write bencoded integer to a binary output.
     *
     * @since 1.0
     */
    static void encode(BEInteger integer, OutputStream out) throws IOException {

        Objects.requireNonNull(integer);

        BigInteger value = integer.getValue();
        write(out, BEParser.INTEGER_PREFIX);
        write(out, Integer.toString(value.intValue()).getBytes(StandardCharsets.UTF_8));
        write(out, BEParser.EOF);
    }

    /**
     * Write bencoded list to a binary output.
     *
     * @since 1.0
     */
    static void encode(BEList list, OutputStream out) throws IOException {

        Objects.requireNonNull(list);

        List<? extends BEObject> values = list.value();
        write(out, BEParser.LIST_PREFIX);

        for (BEObject value : values) {
            value.writeTo(out);
        }

        write(out, BEParser.EOF);
    }

    /**
     * Write bencoded dictionary to a binary output.
     *
     * @since 1.0
     */
    static void encode(BEMap map, OutputStream out) throws IOException {
        Objects.requireNonNull(map);

        write(out, BEParser.MAP_PREFIX);

        TreeMap<byte[], BEObject> values = map.value().entrySet().stream()
                .collect(Collectors.toMap(
                        e -> e.getKey().getBytes(StandardCharsets.UTF_8),
                        Map.Entry::getValue,
                        (u, u2) -> {
                            throw new IllegalStateException();
                        },
                        () -> new TreeMap<>(ByteStringComparator.comparator())));

        for (Map.Entry<byte[], BEObject> e : values.entrySet()) {
            encodeString(e.getKey(), out);
            e.getValue().writeTo(out);
        }

        write(out, BEParser.EOF);
    }

    private static void write(OutputStream out, int i) throws IOException {
        out.write(i);
    }

    private static void write(OutputStream out, byte[] bytes) throws IOException {
        out.write(bytes);
    }
}
