package threads.magnet.net;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Objects;

import threads.magnet.protocol.Protocols;

public record PeerId(byte[] peerId) {

    private static final int PEER_ID_LENGTH = 20;

    private static PeerId createPeerId(byte[] peerId) {
        Objects.requireNonNull(peerId);
        if (peerId.length != PEER_ID_LENGTH) {
            throw new RuntimeException("Illegal peer ID length: " + peerId.length);
        }
        return new PeerId(peerId);
    }

    /**
     * @return Standrad peer ID length in BitTorrent.
     * @since 1.0
     */
    public static int length() {
        return PEER_ID_LENGTH;
    }


    public static PeerId fromBytes(byte[] bytes) {
        return PeerId.createPeerId(bytes);
    }

    /**
     * @return Binary peer ID representation.
     */
    public byte[] getBytes() {
        return peerId;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(peerId);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !PeerId.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        return (obj == this) || Arrays.equals(peerId, ((PeerId) obj).getBytes());
    }

    @NonNull
    @Override
    public String toString() {
        return Protocols.toHex(peerId);
    }
}
