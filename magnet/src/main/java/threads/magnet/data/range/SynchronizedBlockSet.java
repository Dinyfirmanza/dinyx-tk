package threads.magnet.data.range;

import java.util.concurrent.locks.Lock;

import threads.magnet.data.BlockSet;

public record SynchronizedBlockSet(BlockSet delegate, Lock lock) implements BlockSet {


    @Override
    public int blockCount() {
        return delegate.blockCount();
    }

    @Override
    public long blockSize() {
        return delegate.blockSize();
    }

    @Override
    public long lastBlockSize() {
        return delegate.lastBlockSize();
    }

    @Override
    public boolean isPresent(int blockIndex) {
        lock.lock();
        try {
            return delegate.isPresent(blockIndex);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isComplete() {
        lock.lock();
        try {
            return delegate.isComplete();
        } finally {
            lock.unlock();
        }
    }


    @Override
    public void clear() {
        lock.lock();
        try {
            delegate.clear();
        } finally {
            lock.unlock();
        }
    }
}
