package threads.magnet.protocol;

import androidx.annotation.NonNull;

public record Request(int pieceIndex, int offset, int length) implements Message {

    /**
     * @since 1.0
     */
    public Request {

        if (pieceIndex < 0 || offset < 0 || length <= 0) {
            throw new InvalidMessageException("Illegal arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), length (" + length + ")");
        }

    }

    /**
     * @since 1.0
     */
    @Override
    public int pieceIndex() {
        return pieceIndex;
    }

    /**
     * @since 1.0
     */
    @Override
    public int offset() {
        return offset;
    }

    /**
     * @since 1.0
     */
    @Override
    public int length() {
        return length;
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] piece index {" + pieceIndex + "}, offset {" + offset +
                "}, length {" + length + "}";
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.REQUEST_ID;
    }
}
